package com.bbuzzart.canvas.player.ui.detail;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bbuzzart.canvas.network.model.Curations;
import com.bbuzzart.canvas.player.R;
import com.bbuzzart.canvas.player.databinding.FragmentDetailBinding;
import com.bbuzzart.canvas.player.support.widget.ItemHorizonMarginDecoration;
import com.bbuzzart.canvas.player.ui.DetailPlayActivity;

import org.parceler.Parcels;

import java.util.Observable;
import java.util.Observer;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import lombok.Getter;

public class DetailFragment extends Fragment implements Observer {

	public static final String TAG = DetailFragment.class.getSimpleName();

	private Curations.Data.Curation curation;
	private FragmentDetailBinding binding;
	private Unbinder unbinder;

	@Getter
	private DetailViewModel detailViewModel;


	public static DetailFragment newInstance(Curations.Data.Curation curation) {
		DetailFragment fragment = new DetailFragment();
		Bundle args = new Bundle();
		args.putParcelable("curation", Parcels.wrap(curation));
		fragment.setArguments(args);

		return fragment;
	}

	public DetailFragment() {

	}


	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		initBinding(inflater, container);

		return binding.getRoot();
	}

	private void initBinding(LayoutInflater inflater, @Nullable ViewGroup container) {
		detailViewModel = new DetailViewModel(inflater.getContext());
		binding = DataBindingUtil.inflate(inflater, R.layout.fragment_detail, container, false);
		binding.setDetailViewModel(detailViewModel);

		setupObserver(detailViewModel);
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		unbinder = ButterKnife.bind(this, binding.getRoot());

		if (getArguments() != null) {
			this.curation = Parcels.unwrap(getArguments().getParcelable("curation"));
		}

		DetailAdapter adapter = new DetailAdapter();
		adapter.setAdapterListener(pick -> {
			Intent intent = new Intent(getActivity(), DetailPlayActivity.class);
			intent.putExtra("curation", Parcels.wrap(curation));
			intent.putExtra("pick", Parcels.wrap(pick));
			startActivity(intent);
		});
		binding.recyclerView.setAdapter(adapter);

		binding.recyclerView.addItemDecoration(new ItemHorizonMarginDecoration(getActivity()));
		binding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
		binding.recyclerView.setFocusable(false);

		this.getDetailViewModel().fetchCuratorPicks(curation.getCurationId());

		Log.w(TAG, "onViewCreated");
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();

		this.getDetailViewModel().reset();
		unbinder.unbind();
	}


	@Override
	public void update(Observable observable, Object data) {
		if (observable instanceof DetailViewModel) {
			DetailAdapter adapter = (DetailAdapter) binding.recyclerView.getAdapter();
			DetailViewModel viewModel = (DetailViewModel) observable;
			adapter.setPicks(viewModel.getCuratorPicks().getData().getPicks());
		}
	}

	public void setupObserver(Observable observable) {
		observable.addObserver(this);
	}

}
