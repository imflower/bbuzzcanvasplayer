package com.bbuzzart.canvas.player;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Map;

public class CanvasPreference {

	public enum Keys {

		Init("init", false),
		Auth("auth", ""),
		CdnDomain("cdn_domain", "");


		private String key;
		private Object value;


		private Keys(String key, Object value) {
			this.key = key;
			this.value = value;
		}

		public String getKey() {
			return key;
		}

		public void setKey(String key) {
			this.key = key;
		}

		public Object getValue() {
			return value;
		}

		public void setValue(Object value) {
			this.value = value;
		}
	}


	private static final String PREF_NAME = "default.pref";

	private static CanvasPreference instance;

	private Context context;

	private CanvasPreference(Context context) {
		this.context = context;
	}

	public static CanvasPreference getInstance(Context context) {
		if (instance == null) {
			instance = new CanvasPreference(context);
		}

		return instance;
	}

	private SharedPreferences getSharedPreference(Context context) {
		return context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
	}


	public Map<String, ?> getAll() {
		return context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE).getAll();
	}

	public boolean initPreference() throws Exception {
		try {
			for (Keys preference : Keys.values()) {
				putValue(preference, preference.getValue());
			}
			putBoolean(Keys.Init, true);
			return true;
		} catch (Exception e) {
			putBoolean(Keys.Init, false);
			return false;
		}
	}

	public boolean putBoolean(Keys key, Object value) throws Exception {
		return getSharedPreference(context).edit().putBoolean(key.getKey(), (Boolean) value).commit();
	}

	public boolean getBoolean(Keys key) {
		return getSharedPreference(context).getBoolean(key.getKey(), false);
	}


	public boolean putInt(Keys key, Object value) throws Exception {
		return getSharedPreference(context).edit().putInt(key.getKey(), (Integer) value).commit();
	}

	public int getInt(Keys key) {
		return getSharedPreference(context).getInt(key.getKey(), -1);
	}


	public boolean putLong(Keys key, Object value) throws Exception {
		return getSharedPreference(context).edit().putLong(key.getKey(), (Long) value).commit();
	}

	public long getLong(Keys key) {
		return getSharedPreference(context).getLong(key.getKey(), -1L);
	}


	public boolean putString(Keys key, Object value) throws Exception {
		return getSharedPreference(context).edit().putString(key.getKey(), (String) value).commit();
	}

	public String getString(Keys key) {
		return getSharedPreference(context).getString(key.getKey(), null);
	}


	public boolean putFloat(Keys key, Object value) throws Exception {
		return getSharedPreference(context).edit().putFloat(key.getKey(), (Float) value).commit();
	}

	public float getFloat(Keys key) {
		return getSharedPreference(context).getFloat(key.getKey(), -1.0F);
	}

	private void putValue(Keys key, Object value) throws Exception {
		if (value instanceof Boolean) {
			putBoolean(key, value);
		} else if (value instanceof Long) {
			putLong(key, value);
		} else if (value instanceof Integer) {
			putInt(key, value);
		} else if (value instanceof Float) {
			putFloat(key, value);
		} else if (value instanceof String) {
			putString(key, value);
		}
	}

}
