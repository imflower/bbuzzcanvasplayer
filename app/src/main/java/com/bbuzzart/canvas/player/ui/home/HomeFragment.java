package com.bbuzzart.canvas.player.ui.home;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bbuzzart.canvas.player.R;
import com.bbuzzart.canvas.player.databinding.FragmentHomeBinding;
import com.bbuzzart.canvas.player.support.widget.ItemVerticalMarginDecoration;
import com.bbuzzart.canvas.player.ui.DetailActivity;

import org.parceler.Parcels;

import java.util.Observable;
import java.util.Observer;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import lombok.Getter;

public class HomeFragment extends Fragment implements Observer {

	public static final String TAG = HomeFragment.class.getSimpleName();

	private FragmentHomeBinding binding;
	private Unbinder unbinder;

	@Getter
	private HomeViewModel homeViewModel;

	@BindView(R.id.swipeRefreshLayout)
	SwipeRefreshLayout swipeRefreshLayout;


	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		initBinding(inflater, container);

		return binding.getRoot();
	}

	private void initBinding(LayoutInflater inflater, @Nullable ViewGroup container) {
		homeViewModel = new HomeViewModel(inflater.getContext());
		binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);
		binding.setHomeViewModel(homeViewModel);

		setupObserver(homeViewModel);
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		unbinder = ButterKnife.bind(this, binding.getRoot());

		HomeAdapter adapter = new HomeAdapter();
		adapter.setAdapterListener(curation -> {
			// Log.w(TAG, "curation=" + curation.toString());

			Intent intent = new Intent(getActivity(), DetailActivity.class);
			intent.putExtra("curation", Parcels.wrap(curation));
			startActivity(intent);
		});
		binding.recyclerView.setAdapter(adapter);
		binding.recyclerView.addItemDecoration(new ItemVerticalMarginDecoration(getActivity()));
		binding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
		binding.recyclerView.setFocusable(false);

		// init SwipeRefresh
		swipeRefreshLayout.setRefreshing(false);
		swipeRefreshLayout.setColorSchemeResources(R.color.md_green_500, R.color.md_red_500, R.color.md_yellow_500, R.color.md_blue_500);
		swipeRefreshLayout.setOnRefreshListener(() -> {
			new Handler().postDelayed(() -> {}, 5000);
			this.getHomeViewModel().fetchCurations();
		});

		this.getHomeViewModel().fetchCurations();

		Log.w(TAG, "onViewCreated");
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();

		this.getHomeViewModel().reset();
		unbinder.unbind();
	}


	@Override
	public void update(Observable observable, Object data) {
		if (observable instanceof HomeViewModel) {
			HomeAdapter peopleAdapter = (HomeAdapter) binding.recyclerView.getAdapter();
			HomeViewModel homeViewModel = (HomeViewModel) observable;
			peopleAdapter.setCurations(homeViewModel.getCurations().getData().getCurations());
			swipeRefreshLayout.setRefreshing(false);
		}
	}

	public void setupObserver(Observable observable) {
		observable.addObserver(this);
	}

}
