package com.bbuzzart.canvas.player.support.wifi;

import org.parceler.Parcel;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Parcel(Parcel.Serialization.BEAN)
public final class WifiScanResult {

	public String bssid;
	public String ssid;
	public String capabilities;
	public int rssi;
	public int frequency;

	public WifiScanResult() {

	}

	public WifiScanResult(String bssid, String ssid, String capabilities, int rssi, int frequency) {
		this.bssid = bssid;
		this.ssid = ssid;
		this.capabilities = capabilities;
		this.rssi = rssi;
		this.frequency = frequency;
	}

}
