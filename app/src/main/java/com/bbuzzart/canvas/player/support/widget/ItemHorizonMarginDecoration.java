package com.bbuzzart.canvas.player.support.widget;

import android.content.Context;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.bbuzzart.canvas.player.R;

public class ItemHorizonMarginDecoration extends RecyclerView.ItemDecoration {
	private int margin;

	public ItemHorizonMarginDecoration(Context context) {
		this.margin = context.getResources().getDimensionPixelSize(R.dimen.item_margin);
	}

	@Override
	public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
		super.getItemOffsets(outRect, view, parent, state);

		final int itemPosition = parent.getChildAdapterPosition(view);
		if (itemPosition == RecyclerView.NO_POSITION) {
			return;
		}

		final int itemCount = state.getItemCount();
		if (itemPosition == 0) {
			outRect.set(margin, 0, margin / 2, 0);
		} else if (itemCount > 0 && itemPosition == itemCount - 1) {
			outRect.set(0, 0, margin, 0);
		} else {
			outRect.set(0, 0, margin / 2, 0);
		}
	}

}