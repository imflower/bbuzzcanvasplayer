package com.bbuzzart.canvas.player.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.balysv.materialripple.MaterialRippleLayout;
import com.bbuzzart.canvas.network.model.Canvas;
import com.bbuzzart.canvas.network.service.BuzzArtService;
import com.bbuzzart.canvas.player.CanvasApplication;
import com.bbuzzart.canvas.player.R;
import com.bbuzzart.canvas.player.support.application.BaseActivity;

import org.parceler.Parcels;
import org.w3c.dom.Text;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import worker8.com.github.radiogroupplus.RadioGroupPlus;

public class SettingActivity extends BaseActivity {

	public static final String TAG = SettingActivity.class.getSimpleName();

	private CanvasApplication application;
	private CompositeDisposable compositeDisposable = new CompositeDisposable();
	private Canvas canvas;

	@BindView(R.id.rootView)
	RelativeLayout rootView;
	@BindView(R.id.toolbar)
	Toolbar toolbar;
	@BindView(R.id.progressView)
	LinearLayout progressView;
	@BindView(R.id.txtTitle)
	TextView txtTitle;


	@BindView(R.id.editCanvasName)
	EditText editCanvasName;
	@BindView(R.id.txtCanvasDisplayId)
	TextView txtCanvasDisplayId;
	@BindView(R.id.txtCurrentCuration)
	TextView txtCurrentCuration;
	@BindView(R.id.txtCurrentWifi)
	TextView txtCurrentWifi;
	@BindView(R.id.txtCurrentWifiState)
	TextView txtCurrentWifiState;
	@BindView(R.id.rippleConnect)
	MaterialRippleLayout rippleConnect;
	@BindView(R.id.switchLabel)
	Switch switchLabel;


	@BindView(R.id.radioImageShowType)
	RadioGroupPlus radioImageShowType;
	@BindView(R.id.radioImageShowTypeCover)
	RadioButton radioImageShowTypeCover;
	@BindView(R.id.radioImageShowTypeRandom)
	RadioButton radioImageShowTypeRandom;
	// @BindView(R.id.radioImageShowTypeScroll)
	// RadioButton radioImageShowTypeScroll;
	@BindView(R.id.radioImageShowTypeContain)
	RadioButton radioImageShowTypeContain;

	@BindView(R.id.radioImageAnimType)
	RadioGroupPlus radioImageAnimType;
	@BindView(R.id.radioImageAnimTypeSlide)
	RadioButton radioImageAnimTypeSlide;
	@BindView(R.id.radioImageAnimTypeFade)
	RadioButton radioImageAnimTypeFade;


	@BindView(R.id.editShowDurationHour)
	EditText editShowDurationHour;
	@BindView(R.id.editShowDurationMinute)
	EditText editShowDurationMinute;


	@BindView(R.id.radioImageBackground)
	RadioGroupPlus radioImageBackground;
	@BindView(R.id.radioImageBackgroundBlack)
	RadioButton radioImageBackgroundBlack;
	@BindView(R.id.radioImageBackgroundWhite)
	RadioButton radioImageBackgroundWhite;
	@BindView(R.id.radioImageBackgroundGray)
	RadioButton radioImageBackgroundGray;



	@OnClick(R.id.rippleConnect)
	public void rippleConnect() {
		Intent intent = new Intent(application, NetworkActivity.class);
		startActivityForResult(intent, 0x1999);
	}

	@OnClick(R.id.rippleSave)
	public void rippleSave() {
		// TODO Dialog
		new MaterialDialog.Builder(SettingActivity.this)
				.content("변경된 내용을 저장하시겠습니까?")
				.onNegative((dialog, which) -> {
					dialog.dismiss();
				})
				.onPositive((dialog, which) -> {
					canvas.setCanvasName(TextUtils.isEmpty(editCanvasName.getText().toString()) ?
							"" : editCanvasName.getText().toString());

					int hour = TextUtils.isEmpty(editShowDurationHour.getText().toString()) ? 0 :
							Integer.parseInt(editShowDurationHour.getText().toString());

					int minute = TextUtils.isEmpty(editShowDurationMinute.getText().toString()) ? 0 :
							Integer.parseInt(editShowDurationMinute.getText().toString());

					canvas.getCanvasSetting().getInterval().setHour(hour);
					canvas.getCanvasSetting().getInterval().setMinute(minute);


					progressView.setVisibility(View.VISIBLE);

					String auth = application.getAccount().getAuth();
					BuzzArtService service = application.getBuzzArtService();

					Disposable disposable = service.doPostCanvasesModify(auth, canvas.getCanvasId(), canvas)
							.subscribeOn(application.subscribeScheduler())
							.observeOn(AndroidSchedulers.mainThread())
							.subscribe(
									result -> {
										Log.w(TAG, "doPostCanvasesModify Result");
										Log.w(TAG, result.toString());
										if (result.getStatus()) {
											// SUCCESS
											onActionResult();
										} else {
											Log.w(TAG, result.toString());
										}
									},
									throwable -> {
										Log.e(TAG, throwable.toString());
										dialog.dismiss();
										progressView.setVisibility(View.GONE);
									}
							);

					compositeDisposable.add(disposable);
				})
				.positiveText("저장")
				.negativeText("취소")
				.show();
	}

	@OnClick(R.id.rippleDisconnect)
	public void rippleDisconnect() {
		new MaterialDialog.Builder(SettingActivity.this)
				.content("캔버스를 연결 해제하면 [새 캔버스 등록]을 해야 사용이 가능합니다. 연결 해제하시겠습니까?")
				.onNegative((dialog, which) -> {
					dialog.dismiss();
				})
				.onPositive((dialog, which) -> {
					progressView.setVisibility(View.VISIBLE);

					String auth = application.getAccount().getAuth();
					BuzzArtService service = application.getBuzzArtService();

					Disposable disposable = service.doPostCanvasesRelease(auth, canvas.getCanvasId())
							.subscribeOn(application.subscribeScheduler())
							.observeOn(AndroidSchedulers.mainThread())
							.subscribe(
									result -> {
										Log.w(TAG, "rippleDisconnect Result");
										Log.w(TAG, result.toString());
										if (result.getStatus()) {
											// SUCCESS
											onActionResult();
										} else {
											Log.w(TAG, result.toString());

											dialog.dismiss();
											progressView.setVisibility(View.GONE);
										}
									},
									throwable -> {
										Log.e(TAG, throwable.toString());
										dialog.dismiss();
										progressView.setVisibility(View.GONE);
									}
							);

					compositeDisposable.add(disposable);
				})
				.positiveText("확인")
				.negativeText("취소")
				.show();
	}


	@Override
	protected int getLayoutResource() {
		return R.layout.activity_setting;
	}

	@Override
	protected void initVariables(Bundle savedInstanceState) {
		this.application = (CanvasApplication) getApplicationContext();

		this.canvas = Parcels.unwrap(getIntent().getParcelableExtra("canvas"));
	}

	@Override
	protected void initData(Bundle savedInstanceState) {
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowTitleEnabled(false);

		txtTitle.setText(canvas.getCanvasName());

		hideSoftKey();


		editCanvasName.setText(TextUtils.isEmpty(canvas.getCanvasName()) ? "" : canvas.getCanvasName());
		txtCanvasDisplayId.setText(canvas.getCanvasDisplayId());
		txtCurrentCuration.setText(canvas.getCanvasSetting().getData().getTitle());


		if (canvas.getIsConnect()) {
			txtCurrentWifi.setText(canvas.getWifiName());
			txtCurrentWifiState.setVisibility(View.VISIBLE);
			rippleConnect.setVisibility(View.GONE);
		} else {
			txtCurrentWifi.setText("");
			txtCurrentWifiState.setVisibility(View.GONE);
			rippleConnect.setVisibility(View.VISIBLE);
		}

		editShowDurationHour.setText(canvas.getCanvasSetting().getInterval().getHour() + "");
		editShowDurationMinute.setText(canvas.getCanvasSetting().getInterval().getMinute() + "");


		switchLabel.setChecked(canvas.getCanvasSetting().getCaption().getUse());
		switchLabel.setOnCheckedChangeListener((buttonView, isChecked) ->
				canvas.getCanvasSetting().getCaption().setUse(isChecked));

		onChangedRadioImageShowType();
		radioImageShowType.setOnCheckedChangeListener((radioGroupPlus, i) ->
				onChangedRadioImageShowType());

		onChangedRadioImageAnimType();
		radioImageAnimType.setOnCheckedChangeListener((radioGroupPlus, i) ->
				onChangedRadioImageAnimType());

		onChangedRadioImageBackground();
		radioImageBackground.setOnCheckedChangeListener((radioGroupPlus, i) ->
				onChangedRadioImageBackground());
	}

	private void onChangedRadioImageBackground() {
		radioImageBackgroundBlack.setChecked(false);
		radioImageBackgroundWhite.setChecked(false);

		switch (radioImageBackground.getCheckedRadioButtonId()) {
			case R.id.radioImageBackgroundBlack:
				canvas.getCanvasSetting().getStyle().setBackgroundColor("black");
				break;
			case R.id.radioImageBackgroundWhite:
				canvas.getCanvasSetting().getTransition().setType("white");
				break;
			case R.id.radioImageBackgroundGray:
				canvas.getCanvasSetting().getTransition().setType("gray");
				break;
		}

		switch (canvas.getCanvasSetting().getStyle().getBackgroundColor()) {
			case "black":
				radioImageBackgroundBlack.setChecked(true);
				break;
			case "white":
				radioImageBackgroundWhite.setChecked(true);
				break;
			case "gray":
				radioImageBackgroundGray.setChecked(true);
				break;
		}
	}

	private void onChangedRadioImageAnimType() {
		radioImageAnimTypeSlide.setChecked(false);
		radioImageAnimTypeFade.setChecked(false);

		switch (radioImageAnimType.getCheckedRadioButtonId()) {
			case R.id.radioImageAnimTypeSlide:
				canvas.getCanvasSetting().getTransition().setType("slide");
				break;
			case R.id.radioImageAnimTypeFade:
				canvas.getCanvasSetting().getTransition().setType("fade");
				break;
		}

		switch (canvas.getCanvasSetting().getTransition().getType()) {
			case "slide":
				radioImageAnimTypeSlide.setChecked(true);
				break;
			case "fade":
				radioImageAnimTypeFade.setChecked(true);
				break;
		}
	}

	private void onChangedRadioImageShowType() {
		radioImageShowTypeCover.setChecked(false);
		radioImageShowTypeRandom.setChecked(false);
		// radioImageShowTypeScroll.setChecked(false);
		radioImageShowTypeContain.setChecked(false);

		switch (radioImageShowType.getCheckedRadioButtonId()) {
			case R.id.radioImageShowTypeCover :
				canvas.getCanvasSetting().getFit().setType("cover");
				break;
			case R.id.radioImageShowTypeRandom:
				canvas.getCanvasSetting().getFit().setType("random");
				break;
			//case R.id.radioImageShowTypeScroll:
			//	canvas.getCanvasSetting().getFit().setType("scroll");
			//	break;
			case R.id.radioImageShowTypeContain:
				canvas.getCanvasSetting().getFit().setType("contain");
				break;
		}

		switch (canvas.getCanvasSetting().getFit().getType()) {
			case "cover":
				radioImageShowTypeCover.setChecked(true);
				break;
			case "random":
				radioImageShowTypeRandom.setChecked(true);
				break;
			//case "scroll":
			//	radioImageShowTypeScroll.setChecked(true);
			//	break;
			case "contain":
				radioImageShowTypeContain.setChecked(true);
				break;
		}
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				onBackPressed();
				return true;
		}
		return false;
	}

	@Override
	public void onBackPressed() {
		finish();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (resultCode == 0x1999) {
			Log.w(TAG, "received=0x1999");
			onActionResult();
		}
	}


	private void hideSoftKey() {
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		if (imm != null)
			imm.hideSoftInputFromWindow(rootView.getWindowToken(), 0);
	}

	private void onActionResult() {
		Intent intent = new Intent();
		intent.putExtra("disconnected", canvas.getCanvasId());
		setResult(0x1234, intent);

		new Handler().postDelayed(this::finish, 500);
	}

}
