package com.bbuzzart.canvas.player.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.balysv.materialripple.MaterialRippleLayout;
import com.bbuzzart.canvas.network.model.Canvas;
import com.bbuzzart.canvas.network.model.CanvasApply;
import com.bbuzzart.canvas.network.model.Curations;
import com.bbuzzart.canvas.network.model.Pick;
import com.bbuzzart.canvas.network.service.BuzzArtService;
import com.bbuzzart.canvas.player.CanvasApplication;
import com.bbuzzart.canvas.player.R;
import com.bbuzzart.canvas.player.support.application.BaseActivity;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import lombok.Getter;
import lombok.Setter;

public class DetailPlayActivity extends BaseActivity {

	public static final String TAG = DetailPlayActivity.class.getSimpleName();

	private CanvasApplication application;
	private CompositeDisposable compositeDisposable;


	@BindView(R.id.rootView)
	RelativeLayout rootView;
	@BindView(R.id.toolbar)
	Toolbar toolbar;
	@BindView(R.id.txtTitle)
	TextView txtTitle;


	@BindView(R.id.checkBoxAll)
	CheckBox checkBoxAll;
	@BindView(R.id.recyclerViewCanvas)
	RecyclerView recyclerViewCanvas;
	@BindView(R.id.recyclerViewEmpty)
	LinearLayout recyclerViewEmpty;
	@BindView(R.id.progressView)
	LinearLayout progressView;


	@OnClick(R.id.rippleApply)
	public void rippleApply() {
		progressView.setVisibility(View.VISIBLE);

		String auth = application.getAccount().getAuth();
		BuzzArtService service = application.getBuzzArtService();


		CanvasApply apply = new CanvasApply();
		List<Integer> _ids = new ArrayList<>();

		for (Canvas c : canvases) {
			if (c.isChecked())
				_ids.add(c.getCanvasId());
		}

		int[] ids = new int[_ids.size()];
		for (int i = 0; i < ids.length; i++) {
			ids[i] = _ids.get(i);
		}
		apply.setCanvasIds(ids);

		if (pick == null) {
			Log.w(TAG, "CURATION");
			apply.setData(new CanvasApply.Data("curation", curation.getCurationId(), 0));
		} else {
			Log.w(TAG, "PICK");
			apply.setData(new CanvasApply.Data("pick", curation.getCurationId(), pick.getPickId()));
		}


		if (_ids.size() > 0) {
			Disposable disposable = service.doPostCanvasesApply(auth, apply)
					.subscribeOn(application.subscribeScheduler())
					.observeOn(AndroidSchedulers.mainThread())
					.subscribe(
							result -> {
								Log.w(TAG, "rippleApply Result");
								Log.w(TAG, result.toString());
								if (result.getStatus()) {
									// SUCCESS
									finish();
								} else {
									Log.w(TAG, result.toString());
									progressView.setVisibility(View.GONE);
								}
							},
							throwable -> {
								Log.e(TAG, "Error rippleApply", throwable);
								progressView.setVisibility(View.GONE);
							}
					);

			compositeDisposable.add(disposable);
		} else {
			Toast.makeText(application, R.string.msg_apply_canvas_select, Toast.LENGTH_SHORT).show();
		}

	}


	private List<Canvas> canvases;
	private Curations.Data.Curation curation;
	private Pick pick;


	@Override
	protected int getLayoutResource() {
		return R.layout.activity_canvas_play;
	}

	@Override
	protected void initVariables(Bundle savedInstanceState) {
		this.application = (CanvasApplication) getApplicationContext();
		compositeDisposable = new CompositeDisposable();

		if (getIntent() != null) {
			this.curation = Parcels.unwrap(getIntent().getParcelableExtra("curation"));
			if (getIntent().hasExtra("pick")) {
				this.pick = Parcels.unwrap(getIntent().getParcelableExtra("pick"));
			}
		}
	}

	@Override
	protected void initData(Bundle savedInstanceState) {
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowTitleEnabled(false);

		txtTitle.setText(pick == null ? curation.getCurationTitle() : pick.getWorkTitle());

		canvases = new ArrayList<>();
		checkBoxAll.setOnCheckedChangeListener((buttonView, isChecked) -> {
			for (int i = 0; i < canvases.size(); i++) {
				canvases.get(i).setChecked(isChecked);
			}
			recyclerViewCanvas.getAdapter().notifyDataSetChanged();
		});

		fetchCanvases();
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				onBackPressed();
				return true;
		}
		return false;
	}

	@Override
	public void onBackPressed() {
		finish();
	}


	private void fetchCanvases() {
		progressView.setVisibility(View.VISIBLE);

		String auth = application.getAccount().getAuth();
		BuzzArtService service = application.getBuzzArtService();

		Disposable disposable = service.fetchCanvases(auth, true, 1, 10)
				.subscribeOn(application.subscribeScheduler())
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(
						result -> {
							Log.w(TAG, "fetchCanvases Result");
							Log.w(TAG, result.toString());
							if (result.getStatus()) {
								// SUCCESS
								canvases = result.getData().getCanvases();

								if (canvases.size() > 0) {
									CanvasAdapter canvasAdapter = new CanvasAdapter(application, result.getData().getCanvases());
									recyclerViewCanvas.setAdapter(canvasAdapter);
									recyclerViewCanvas.setLayoutManager(new LinearLayoutManager(DetailPlayActivity.this));
									recyclerViewCanvas.setFocusable(false);
									recyclerViewCanvas.setVisibility(View.VISIBLE);
									recyclerViewEmpty.setVisibility(View.GONE);
								} else {
									recyclerViewCanvas.setVisibility(View.GONE);
									recyclerViewEmpty.setVisibility(View.VISIBLE);
								}
							} else {
								Log.w(TAG, result.toString());
							}

							progressView.setVisibility(View.GONE);
						},
						throwable -> {
							Log.e(TAG, throwable.toString());
							progressView.setVisibility(View.GONE);
						}
				);

		compositeDisposable.add(disposable);
	}


	class CanvasAdapter extends RecyclerView.Adapter<DetailPlayActivity.CanvasAdapter.CanvasViewHolder> {

		@Getter
		@Setter
		List<Canvas> canvases;

		Context context;

		public CanvasAdapter(Context context, List<Canvas> canvases) {
			this.context = context;
			this.canvases = canvases;
		}

		@NonNull
		@Override
		public CanvasAdapter.CanvasViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
			View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view_play_canvas, parent, false);
			return new CanvasAdapter.CanvasViewHolder(view);
		}

		@Override
		public void onBindViewHolder(@NonNull CanvasAdapter.CanvasViewHolder holder, int position) {
			final Canvas canvas = canvases.get(holder.getAdapterPosition());

			holder.txtCanvasName.setText(canvas.getCanvasName());

			if (canvas.getCanvasSetting().getData().getType().equals("curation")) {
				holder.txtCanvasCurationType.setText(R.string.msg_canvas_curation_type_curation);
			} else {
				holder.txtCanvasCurationType.setText(R.string.msg_canvas_curation_type_work);
			}
			holder.txtCanvasCurationName.setText(canvas.getCanvasSetting().getData().getTitle());

			holder.checkBox.setChecked(canvas.isChecked());
			holder.checkBox.setOnCheckedChangeListener((buttonView, isChecked) -> canvas.setChecked(isChecked));
		}

		@Override
		public int getItemCount() {
			return canvases.size();
		}

		@Override
		public long getItemId(int position) {
			return canvases.get(position).getCanvasId();
		}

		class CanvasViewHolder extends RecyclerView.ViewHolder {
			@BindView(R.id.rippleItem)
			MaterialRippleLayout rippleItem;
			@BindView(R.id.txtCanvasName)
			TextView txtCanvasName;
			@BindView(R.id.txtCanvasCurationType)
			TextView txtCanvasCurationType;
			@BindView(R.id.txtCanvasCurationName)
			TextView txtCanvasCurationName;
			@BindView(R.id.checkBox)
			CheckBox checkBox;

			CanvasViewHolder(View itemView) {
				super(itemView);
				ButterKnife.bind(this, itemView);
			}
		}
	}

}
