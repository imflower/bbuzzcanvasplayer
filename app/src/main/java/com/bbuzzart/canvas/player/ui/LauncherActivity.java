package com.bbuzzart.canvas.player.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Toast;

import com.bbuzzart.canvas.network.service.BuzzArtService;
import com.bbuzzart.canvas.player.CanvasApplication;
import com.bbuzzart.canvas.player.CanvasPreference;
import com.bbuzzart.canvas.player.R;
import com.bbuzzart.canvas.player.support.application.BaseActivity;
import com.easyandroidanimations.library.FadeInAnimation;
import com.easyandroidanimations.library.FadeOutAnimation;
import com.easyandroidanimations.library.TransferAnimation;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class LauncherActivity extends BaseActivity {

	private static final String TAG = LauncherActivity.class.getSimpleName();

	private static final int GOOGLE_SIGN_IN_RESULT = 0x0001;
	private static String GOOGLE_CLIENT_ID = "";
	private static String GOOGLE_CLIENT_SECRET = "";


	private CanvasApplication application;
	private CanvasPreference preference;
	private CompositeDisposable compositeDisposable;
	private GoogleSignInClient mGoogleSignInClient;
	private CallbackManager callbackManager;


	@BindView(R.id.logo)
	ImageView logo;
	@BindView(R.id.logoImage)
	ImageView logoImage;
	@BindView(R.id.logoInScroll)
	ImageView logoInScroll;
	@BindView(R.id.logoTextImage)
	ImageView logoTextImage;


	@BindView(R.id.rootView)
	FrameLayout rootView;
	@BindView(R.id.logoLayout)
	FrameLayout logoLayout;
	@BindView(R.id.scrollView)
	ScrollView scrollView;
	@BindView(R.id.progressLayout)
	FrameLayout progressLayout;


	@BindView(R.id.editEmail)
	EditText editEmail;
	@BindView(R.id.editPassword)
	EditText editPassword;


	@Override
	protected int getLayoutResource() {
		return R.layout.activity_launcher;
	}

	@Override
	protected void initVariables(Bundle savedInstanceState) {
		initPreference();
	}

	@Override
	protected void initData(Bundle savedInstanceState) {
		application = (CanvasApplication) getApplicationContext();
		scrollView.setVisibility(View.GONE);

		editEmail.setText("");
		editPassword.setText("");


		hideSoftKey();
		initSign();
		new Handler().postDelayed(this::initServer, 500);


		// TODO GLOBAL
		rootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
			@Override
			public void onGlobalLayout() {
				try {
					Rect r = new Rect();
					rootView.getWindowVisibleDisplayFrame(r);
					int screenHeight = rootView.getRootView().getHeight();
					int keypadHeight = screenHeight - r.bottom;

					// Log.d(TAG, "screenHeight=" + screenHeight + ", keypadHeight=" + keypadHeight);
					if (keypadHeight > screenHeight * 0.15) {
						// keyboard is opened
						Log.e(TAG, "keyboard is open *show");
						// showKeyboard = true;

						logoInScroll.setVisibility(View.GONE);
					} else {
						// keyboard is closed
						Log.e(TAG, "keyboard is closed *hide");
						// showKeyboard = false;

						logoInScroll.setVisibility(View.VISIBLE);
					}
				} catch (Exception e) {
					// NOPE
				}
			}
		});
	}


	private static final long FINISH_INTERVAL_TIME = 1000L;
	private long backPressedTime;

	@Override
	public void onBackPressed() {
		long tempTime = System.currentTimeMillis();
		long intervalTime = tempTime - backPressedTime;
		if (0 <= intervalTime && FINISH_INTERVAL_TIME >= intervalTime) {
			finish();
		} else {
			backPressedTime = tempTime;
			Toast.makeText(getApplicationContext(), getString(R.string.app_finish), Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		callbackManager.onActivityResult(requestCode, resultCode, data);
		Log.w(TAG, "requestCode == " + requestCode);

		if (requestCode == GOOGLE_SIGN_IN_RESULT) {
			Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
			handleSignInResult(task);
		}
	}

	@Override
	protected void onDestroy() {
		if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
			compositeDisposable.dispose();
		}

		compositeDisposable = null;

		super.onDestroy();
	}

	private void hideSoftKey() {
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		if (imm != null)
			imm.hideSoftInputFromWindow(rootView.getWindowToken(), 0);
	}

	private void initPreference() {
		preference = CanvasPreference.getInstance(getApplicationContext());
		if (!preference.getBoolean(CanvasPreference.Keys.Init)) {
			try {
				Log.e(TAG, "initPreference");
				preference.initPreference();
			} catch (Exception e) {
				// SKIP
			}
		} else {
			Log.e(TAG, "initPreference skip");
		}
	}

	private void initSign() {
		compositeDisposable = new CompositeDisposable();

		GOOGLE_CLIENT_ID = getString(R.string.google_client_id);
		GOOGLE_CLIENT_SECRET = getString(R.string.google_client_secret);

		GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
				.requestEmail()
				.requestIdToken(GOOGLE_CLIENT_ID)
				.requestServerAuthCode(GOOGLE_CLIENT_ID)
				.build();

		this.mGoogleSignInClient = GoogleSignIn.getClient(application, gso);
		this.mGoogleSignInClient.signOut();


		this.callbackManager = CallbackManager.Factory.create();
		LoginManager.getInstance().logOut();
	}

	private void initServer() {
		CanvasApplication application = (CanvasApplication) getApplicationContext();
		BuzzArtService service = application.getBuzzArtService();

		Disposable disposable = service.fetchInitialize()
				.subscribeOn(application.subscribeScheduler())
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(
						result -> {
							Log.w(TAG, "Initialize Result");
							Log.w(TAG, result.toString());
							if (result.getStatus()) {
								// SUCCESS
								application.setBuzzArtServerConfig(result);
								preference.putString(CanvasPreference.Keys.CdnDomain, result.getData().getCdn().getDomain());
								Log.w(TAG, "CDN=" + application.getBuzzArtServerConfig().getData().getCdn().getDomain());

								initAccount();
							} else {
								Log.w(TAG, result.toString());
							}
						},
						throwable -> Log.e(TAG, throwable.toString())
				);

		compositeDisposable.add(disposable);
	}


	@OnClick(R.id.rippleSignInWithEmail)
	public void rippleSignInWithEmail() {
		String email = editEmail.getText() == null ? "" : editEmail.getText().toString();
		String password = editPassword.getText() == null ? "" : editPassword.getText().toString();

		if (!TextUtils.isEmpty(email) && !TextUtils.isEmpty(password)) {
			// TODO LOGIN
			hideSoftKey();
			progressLayout.setVisibility(View.VISIBLE);

			CanvasApplication application = (CanvasApplication) getApplicationContext();
			BuzzArtService service = application.getBuzzArtService();

			Disposable disposable = service.doPostEmailSign(email, password)
					.subscribeOn(application.subscribeScheduler())
					.observeOn(AndroidSchedulers.mainThread())
					.subscribe(
							result -> {
								Log.w(TAG, "Email Sign Result");
								Log.w(TAG, result.toString());
								if (result.getStatus()) {
									// SUCCESS
									application.setAccount(result.getData().getAccount());
									CanvasPreference.getInstance(getApplicationContext())
											.putString(CanvasPreference.Keys.Auth, result.getData().getAccount().getAuth());

									new Handler().postDelayed(() -> {
										startActivity(new Intent(getApplicationContext(), HomeActivity.class));
										finish();
									}, 500);
								} else {
									Log.e(TAG, result.toString());
									progressLayout.setVisibility(View.GONE);
									Toast.makeText(application, R.string.msg_invalid_user_info, Toast.LENGTH_SHORT).show();
								}
							},
							throwable -> {
								Log.e(TAG, throwable.toString());
								progressLayout.setVisibility(View.GONE);
							}
					);

			compositeDisposable.add(disposable);
		} else {
			Toast.makeText(application, R.string.msg_inpit_email_or_password, Toast.LENGTH_SHORT).show();
		}
	}

	@OnClick(R.id.rippleSignGoogle)
	public void rippleSignGoogle() {
		try {
			Intent signInIntent = mGoogleSignInClient.getSignInIntent();
			startActivityForResult(signInIntent, GOOGLE_SIGN_IN_RESULT);
		} catch (Exception e) {
			Log.e(TAG, "Google Sign Error", e);
		}
	}

	@OnClick(R.id.rippleSignFacebook)
	public void rippleSignFacebook() {
		LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));
		LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {

			@Override
			public void onSuccess(final LoginResult loginResult) {
				Log.e(TAG, "Facebook graph API called success");
				GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), (user, response) -> {
					if (response.getError() != null) {
						Log.w("", response.getError().toString());
					} else {
						Log.i(TAG, "user: " + user.toString());
						Log.i(TAG, "AccessToken: " + loginResult.getAccessToken().getToken());

						try {
							String email = user.getString("email");
							String userName = user.getString("name");
							String id = user.getString("id");
							String token = loginResult.getAccessToken().getToken();

							BuzzArtService service = application.getBuzzArtService();
							Disposable disposable = service.doPostFacebookSign(email, userName, id, token)
									.subscribeOn(application.subscribeScheduler())
									.observeOn(AndroidSchedulers.mainThread())
									.subscribe(
											result -> {
												if (result.getStatus()) {
													// SUCCESS
													application.setAccount(result.getData().getAccount());
													CanvasPreference.getInstance(getApplicationContext())
															.putString(CanvasPreference.Keys.Auth, result.getData().getAccount().getAuth());

													new Handler().postDelayed(() -> {
														startActivity(new Intent(getApplicationContext(), HomeActivity.class));
														finish();
													}, 100);
													Log.w(TAG, "Facebook SignIn Success = " + result.toString());
												} else {
													Log.e(TAG, result.toString());
												}
											},
											throwable -> Log.e(TAG, throwable.toString())
									);

							compositeDisposable.add(disposable);
						} catch (JSONException e) {
							Handler handler = new Handler(Looper.getMainLooper());
							handler.post(() -> {
								Toast.makeText(application, "Facebook login error:" + e.getMessage(), Toast.LENGTH_SHORT).show();
							});

							Log.e(TAG, "FACEBOOK", e);
						}
					}
				});
				Bundle parameters = new Bundle();
				parameters.putString("fields", "id,name,email");
				request.setParameters(parameters);
				request.executeAsync();
			}

			@Override
			public void onError(FacebookException error) {
				Log.e(TAG, "Error: " + error);

				Handler handler = new Handler(Looper.getMainLooper());
				handler.post(() -> {
					Toast.makeText(application, "Facebook login error:" + error.getMessage(), Toast.LENGTH_SHORT).show();
				});
			}

			@Override
			public void onCancel() {
				Handler handler = new Handler(Looper.getMainLooper());
				handler.post(() -> {
					Toast.makeText(application, "Facebook login cancel", Toast.LENGTH_SHORT).show();
				});

			}
		});
	}


	@SuppressWarnings("ConstantConditions")
	private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
		try {
			GoogleSignInAccount account = completedTask.getResult(ApiException.class);
			Log.w(TAG, "Google Account == " + account.getEmail());

			final RequestBody requestBody = new FormBody.Builder()
					.add("code", account.getServerAuthCode())
					.add("client_id", GOOGLE_CLIENT_ID)
					.add("client_secret", GOOGLE_CLIENT_SECRET)
					// .add("redirect_uri", "https://bbuzzart-app.firebaseapp.com/__/auth/handler")
					.add("grant_type", "authorization_code")
					.build();
			final Request request = new Request.Builder()
					.url("https://www.googleapis.com/oauth2/v4/token")
					.post(requestBody)
					.build();

			final OkHttpClient client = new OkHttpClient();
			client.newCall(request).enqueue(new Callback() {
				@Override
				public void onFailure(@NonNull Call call, @NonNull IOException e) {
					Log.e(TAG, e.toString());
				}

				@Override
				public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
					try {
						JSONObject jsonObject = new JSONObject(response.body().string());
						final String message = jsonObject.toString(5);
						Log.w(TAG, message);

						String email = account.getEmail();
						String userName = account.getDisplayName();
						String id = account.getId();
						String token = jsonObject.getString("access_token");

						BuzzArtService service = application.getBuzzArtService();

						Disposable disposable = service.doPostGoogleSign(email, userName, id, token)
								.subscribeOn(application.subscribeScheduler())
								.observeOn(AndroidSchedulers.mainThread())
								.subscribe(
										result -> {
											if (result.getStatus()) {
												// SUCCESS
												Log.w(TAG, "Google SignIn Success = " + result.toString());
												application.setAccount(result.getData().getAccount());
												preference.putString(CanvasPreference.Keys.Auth, result.getData().getAccount().getAuth());

												new Handler().postDelayed(() -> {
													startActivity(new Intent(getApplicationContext(), HomeActivity.class));
													finish();
												}, 100);
											} else {
												Log.e(TAG, result.toString());
											}
										},
										throwable -> Log.e(TAG, throwable.toString())
								);

						compositeDisposable.add(disposable);
					} catch (JSONException e) {
						Handler handler = new Handler(Looper.getMainLooper());
						handler.post(() -> {
							Toast.makeText(application, "Google Sign Failed : [" + e.getMessage() + "]", Toast.LENGTH_SHORT).show();
						});
						Log.e(TAG, "FACEBOOK", e);
					}
				}
			});

		} catch (ApiException e) {
			Log.e(TAG, "signInResult:failed code=" + e);
		}
	}


	private void initAccount() {
		String auth = preference.getString(CanvasPreference.Keys.Auth);
		Log.w(TAG, "AUTH=" + auth);
		if (!TextUtils.isEmpty(auth)) {
			// TODO LOGIN
			CanvasApplication application = (CanvasApplication) getApplicationContext();
			BuzzArtService service = application.getBuzzArtService();

			Disposable disposable = service.doPostAuthSign(auth)
					.subscribeOn(application.subscribeScheduler())
					.observeOn(AndroidSchedulers.mainThread())
					.subscribe(
							result -> {
								Log.w(TAG, "Auth Sign Result");
								Log.w(TAG, result.toString());
								if (result.getStatus()) {
									// SUCCESS
									application.setAccount(result.getData().getAccount());

									new Handler().postDelayed(() -> {
										startActivity(new Intent(getApplicationContext(), HomeActivity.class));
										finish();
									}, 500);
								} else {
									Log.e(TAG, result.toString());
								}
							},
							throwable -> Log.e(TAG, throwable.toString())
					);

			compositeDisposable.add(disposable);
		} else {
			new Handler().postDelayed(() -> {
				runOnUiThread(() -> {
					new FadeOutAnimation(logoTextImage).setDuration(500).animate();
					new TransferAnimation(logoImage).setDestinationView(logo).setDuration(1500).animate();
					new Handler().postDelayed(() -> new FadeInAnimation(scrollView).setDuration(1000).animate(), 500);
					new Handler().postDelayed(() -> new FadeOutAnimation(logoLayout).setDuration(1000).animate(), 1000);
				});
			}, 2000);
		}
	}

	/*
	private boolean doValidateEmail() {
		if (!TextUtils.isEmpty(editEmail.getText())) {
			String regexEmail = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]{0,10})*@[A-Za-z0-9]+(\\.[A-Za-z0-9]{0,10})*(\\.[A-Za-z]{0,5})$";

			String email = editEmail.getText().toString();
			if (!email.matches(regexEmail)) {
				editEmail.setError("이메일 형식이 맞지 않습니다.");

				return false;
			} else {
				editEmail.setError(null);

				return true;
			}
		} else {
			return false;
		}
	}
	*/

}
