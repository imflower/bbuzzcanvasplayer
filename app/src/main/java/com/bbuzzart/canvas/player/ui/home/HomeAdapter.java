package com.bbuzzart.canvas.player.ui.home;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.bbuzzart.canvas.player.databinding.ItemViewHomeBinding;
import com.bbuzzart.canvas.network.model.Curations;
import com.bbuzzart.canvas.player.R;

import java.util.Collections;
import java.util.List;

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.CuratorAdapterViewHolder> {

	private static final String TAG = HomeAdapter.class.getSimpleName();

	private List<Curations.Data.Curation> curations;

	public interface AdapterListener {
		void onSelected(Curations.Data.Curation curation);
	}

	private AdapterListener adapterListener;

	public void setAdapterListener(AdapterListener adapterListener) {
		this.adapterListener = adapterListener;
	}

	HomeAdapter() {
		this.curations = Collections.emptyList();
	}

	@NonNull
	@Override
	public CuratorAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		final ItemViewHomeBinding itemViewCurationBinding =
				DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_view_home, parent, false);

		return new CuratorAdapterViewHolder(itemViewCurationBinding);
	}

	@Override
	public void onBindViewHolder(@NonNull CuratorAdapterViewHolder holder, int position) {
		holder.bindCuration(curations.get(position));
		holder.itemViewHomeBinding.cardView.setOnClickListener(v -> {
			long curationId = holder.itemViewHomeBinding.getHomeItemViewModel().getCuration().getCurationId();
			Log.w(TAG, "curationId=" + curationId);

			adapterListener.onSelected(holder.itemViewHomeBinding.getHomeItemViewModel().getCuration());
		});
	}


	@Override
	public int getItemCount() {
		return (null != curations ? curations.size() : 0);
	}

	@Override
	public long getItemId(int position) {
		return curations.get(position).getCurationId();
	}

	void setCurations(List<Curations.Data.Curation> curations) {
		this.curations = curations;
		notifyDataSetChanged();
	}


	static class CuratorAdapterViewHolder extends RecyclerView.ViewHolder {
		ItemViewHomeBinding itemViewHomeBinding;

		CuratorAdapterViewHolder(ItemViewHomeBinding itemViewHomeBinding) {
			super(itemViewHomeBinding.cardView);
			this.itemViewHomeBinding = itemViewHomeBinding;
		}

		void bindCuration(Curations.Data.Curation curation) {
			itemViewHomeBinding.setHomeItemViewModel(new HomeItemViewModel(curation));
		}
	}

}
