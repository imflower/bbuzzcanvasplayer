package com.bbuzzart.canvas.player.support.application;

import android.app.ActivityManager;
import android.content.Context;
import android.util.Log;

public class AppUtils {

	public static boolean isServiceRunning(String TAG, Class<?> serviceClass, Context context) {
		ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		if (manager != null) {
			for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
				if (serviceClass.getName().equals(service.service.getClassName())) {
					Log.w(TAG, "service running");
					return true;
				}
			}
		}

		Log.w(TAG, "service not running");
		return false;
	}

}
