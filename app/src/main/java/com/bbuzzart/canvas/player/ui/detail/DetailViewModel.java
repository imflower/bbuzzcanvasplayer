package com.bbuzzart.canvas.player.ui.detail;

import android.content.Context;
import android.databinding.ObservableInt;
import android.support.annotation.NonNull;
import android.util.Log;

import com.bbuzzart.canvas.network.model.CuratorPicks;
import com.bbuzzart.canvas.network.service.BuzzArtService;
import com.bbuzzart.canvas.player.CanvasApplication;

import java.util.Observable;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import lombok.Getter;

public class DetailViewModel extends Observable {

	private static final String TAG = DetailViewModel.class.getSimpleName();

	public ObservableInt recyclerView;

	private Context context;

	@Getter
	private CuratorPicks curatorPicks;

	private CompositeDisposable compositeDisposable = new CompositeDisposable();

	public DetailViewModel(@NonNull Context context) {
		this.context = context;
		this.curatorPicks = new CuratorPicks();
	}

	public void fetchCuratorPicks(int curationId) {
		CanvasApplication application = CanvasApplication.create(context);
		BuzzArtService service = application.getBuzzArtService();

		Disposable disposable = service.fetchCuratorPicks(curationId, application.getAccount().getAuth(), true, 1, 10)
				.subscribeOn(application.subscribeScheduler())
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(
						curatorPicks -> {
							if (curatorPicks.getStatus()) {
								Log.w(TAG, curatorPicks.getData().getPagination().toString());

								// TODO success
								this.curatorPicks.getData().getPicks().clear();
								this.curatorPicks.getData().setPagination(curatorPicks.getData().getPagination());
								this.curatorPicks.getData().getPicks().addAll(curatorPicks.getData().getPicks());

								setChanged();
								notifyObservers();
							} else {
								// TODO fail
								Log.e("Model", curatorPicks.toString());
							}
						},
						throwable -> {
							Log.e("Model", "Fetch Error", throwable);
						}
				);
		compositeDisposable.add(disposable);
	}


	public void reset() {
		if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
			compositeDisposable.dispose();
		}

		compositeDisposable = null;
		context = null;
	}
}
