package com.bbuzzart.canvas.player.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.DhcpInfo;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.internal.MDTintHelper;
import com.afollestad.materialdialogs.internal.ThemeSingleton;
import com.balysv.materialripple.MaterialRippleLayout;
import com.bbuzzart.canvas.player.CanvasApplication;
import com.bbuzzart.canvas.player.CanvasPreference;
import com.bbuzzart.canvas.player.R;
import com.bbuzzart.canvas.player.support.application.BaseActivity;
import com.bbuzzart.canvas.player.support.wifi.NetworkType;
import com.bbuzzart.canvas.player.support.wifi.NetworkUtils;
import com.bbuzzart.canvas.player.support.wifi.WifiConfigManager;
import com.bbuzzart.canvas.player.support.wifi.WifiScanResult;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import lombok.Getter;
import lombok.Setter;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class NetworkActivity extends BaseActivity {

	public static final String TAG = NetworkActivity.class.getSimpleName();

	private CanvasApplication application;

	private Handler handler;
	private WifiManager wifiManager;
	private BroadcastReceiver broadcastReceiver;

	private boolean isApScanning;
	private boolean isHotspotConnect;


	@BindView(R.id.rootView)
	RelativeLayout rootView;
	@BindView(R.id.toolbar)
	Toolbar toolbar;
	@BindView(R.id.recyclerViewCanvas)
	RecyclerView recyclerViewCanvas;
	@BindView(R.id.recyclerViewEmpty)
	LinearLayout recyclerViewEmpty;
	@BindView(R.id.progressView)
	LinearLayout progressView;


	@OnClick(R.id.rippleRefresh)
	public void rippleRefresh() {
		progressView.setVisibility(View.VISIBLE);

		isApScanning = true;
		isHotspotConnect = false;

		handler.post(WifiTurnOn);
	}

	@Override
	protected int getLayoutResource() {
		return R.layout.activity_network;
	}

	@Override
	protected void initVariables(Bundle savedInstanceState) {
		this.application = (CanvasApplication) getApplicationContext();

		if (wifiManager == null) {
			wifiManager = (WifiManager) application.getSystemService(Context.WIFI_SERVICE);
		}

		handler = new Handler();

		initIntentFilter();
	}

	@Override
	protected void initData(Bundle savedInstanceState) {
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowTitleEnabled(false);


		rippleRefresh();
	}

	@Override
	protected void onDestroy() {
		if (broadcastReceiver != null)
			unregisterReceiver(broadcastReceiver);

		super.onDestroy();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				onBackPressed();
				return true;
		}
		return false;
	}

	@Override
	public void onBackPressed() {
		finish();
	}


	private WifiScanResult canvasWifiScanResult;
	private List<WifiScanResult> wifiResults;
	private List<WifiScanResult> canvasResults;

	private WifiInfo canvasWifiInfo;
	private DhcpInfo canvasDhcpInfo;

	private MaterialDialog dialogWifiScanResult;

	private void initIntentFilter() {
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
		intentFilter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);

		broadcastReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				String action = intent.getAction();
				if (action != null) {
					switch (action) {
						case WifiManager.NETWORK_STATE_CHANGED_ACTION: {
							if (!isHotspotConnect)
								return;

							NetworkInfo info = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);

							Log.w(TAG, "NetInfo=" + info.toString());

							if (info.isConnected()) {
								isHotspotConnect = false;

								canvasWifiInfo = wifiManager.getConnectionInfo();
								canvasDhcpInfo = wifiManager.getDhcpInfo();

								Log.w(TAG, "WIFI_INFO=" + canvasWifiInfo.toString());
								Log.e(TAG, "DHCP_INFO=" + canvasDhcpInfo.toString());


								dialogWifiScanResult = new MaterialDialog.Builder(NetworkActivity.this)
										.title(canvasWifiScanResult.getSsid())
										.titleColorRes(R.color.md_red_500)
										.autoDismiss(false)
										.content(R.string.msg_select_nearby_wifi)
										.adapter(new CanvasWifiAdapter(wifiResults), null)
										.onNegative((dialog, which) -> {
											dialog.dismiss();
										})
										.negativeText(getString(R.string.msg_cancel))
										.show();
							} else {
								// wifi connection was lost
							}
							break;
						}
						case WifiManager.SCAN_RESULTS_AVAILABLE_ACTION: {
							if (!isApScanning) {
								return;
							}

							Log.i(TAG, "<< Received Wi-fi Scan Result >> ");

							isApScanning = false;
							progressView.setVisibility(View.GONE);

							if (wifiManager.getScanResults() != null && wifiManager.getScanResults().size() > 0) {
								wifiResults = new ArrayList<>();
								canvasResults = new ArrayList<>();

								for (ScanResult result : wifiManager.getScanResults()) {
									if (!TextUtils.isEmpty(result.SSID) && !TextUtils.isEmpty(result.BSSID) && result.frequency < 2500) {
										WifiScanResult scanResult = new WifiScanResult(
												result.BSSID, result.SSID, result.capabilities, result.level, result.frequency);

										if (result.SSID.startsWith("BBuzzCanvas")) {
											canvasResults.add(scanResult);
											Log.w(TAG, result.toString());
										} else {
											wifiResults.add(scanResult);
										}
									}
								}

								if (canvasResults.size() > 0) {
									Collections.sort(wifiResults, (o1, o2) -> {
										if (o1.getRssi() < o2.getRssi()) {
											return 1;
										} else if (o1.getRssi() > o2.getRssi()) {
											return -1;
										} else {
											return 0;
										}
									});

									CanvasAdapter adapter = new CanvasAdapter(application, canvasResults, wifiResults);
									recyclerViewCanvas.setAdapter(adapter);
									recyclerViewCanvas.setLayoutManager(new LinearLayoutManager(NetworkActivity.this));
									recyclerViewCanvas.setFocusable(false);
									recyclerViewCanvas.setVisibility(View.VISIBLE);
									recyclerViewEmpty.setVisibility(View.GONE);
								} else {
									recyclerViewCanvas.setVisibility(View.GONE);
									recyclerViewEmpty.setVisibility(View.VISIBLE);

									Toast.makeText(getApplicationContext(), R.string.msg_not_found_canvas, Toast.LENGTH_SHORT).show();
								}
							} else {
								recyclerViewCanvas.setVisibility(View.GONE);
								recyclerViewEmpty.setVisibility(View.VISIBLE);

								Toast.makeText(getApplicationContext(), R.string.msg_not_found_canvas, Toast.LENGTH_SHORT).show();
							}
						}
					}
				}
			}
		};


		registerReceiver(broadcastReceiver, intentFilter);
	}

	Runnable WifiTurnOn = new Runnable() {
		@Override
		public void run() {
			int count;
			try {
				count = 0;
				wifiManager.setWifiEnabled(true);
				Log.i(TAG, "Wi-fi enabled");

				while (!wifiManager.isWifiEnabled()) {
					if (count >= 20) {
						throw new Exception("WIFI Enable Failed");
					}
					Log.i(TAG, "Still waiting for wi-fi to enabling...");
					Thread.sleep(500L);
					count++;
				}

				isApScanning = true;
				wifiManager.startScan();

			} catch (Exception e) {
				Log.e(TAG, "WifiTurnOn Error", e);
			}
		}
	};

	Runnable WifiTurnOff = new Runnable() {
		@Override
		public void run() {
			int count;
			try {
				isApScanning = false;
				count = 0;
				wifiManager.setWifiEnabled(false);
				Log.i(TAG, "Wi-fi disabling");

				while (wifiManager.isWifiEnabled()) {
					if (count >= 20) {
						throw new Exception("WIFI Disable Failed");
					}
					Log.i(TAG, "Still waiting for wi-fi to disabling...");
					Thread.sleep(500L);
					count++;
				}
			} catch (Exception e) {
				Log.e(TAG, "WifiTurnOn Error", e);
			}
		}
	};


	class CanvasAdapter extends RecyclerView.Adapter<CanvasAdapter.CanvasViewHolder> {

		@Getter
		@Setter
		List<WifiScanResult> canvasResults;

		@Getter
		@Setter
		List<WifiScanResult> wifiResults;

		Context context;

		public CanvasAdapter(Context context, List<WifiScanResult> canvasResults, List<WifiScanResult> wifiResults) {
			this.context = context;
			this.canvasResults = canvasResults;
			this.wifiResults = wifiResults;
		}


		@NonNull
		@Override
		public CanvasViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
			View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view_network_canvas, parent, false);
			return new CanvasViewHolder(view);
		}

		@Override
		public void onBindViewHolder(@NonNull CanvasViewHolder holder, int position) {
			final WifiScanResult result = canvasResults.get(holder.getAdapterPosition());

			isHotspotConnect = true;
			canvasWifiScanResult = result;

			holder.txtCanvasName.setText(result.getSsid());
			holder.rippleItem.setOnClickListener((view) -> {
				Log.w(TAG, "Canvas WIFI == " + result.toString());

				WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
				WifiConfigManager.configure(wifiManager, result.ssid, null, "nopass");
			});
		}

		@Override
		public int getItemCount() {
			return canvasResults.size();
		}


		class CanvasViewHolder extends RecyclerView.ViewHolder {
			@BindView(R.id.rippleItem)
			MaterialRippleLayout rippleItem;
			@BindView(R.id.txtCanvasName)
			TextView txtCanvasName;

			CanvasViewHolder(View itemView) {
				super(itemView);
				ButterKnife.bind(this, itemView);
			}
		}
	}



	class CanvasWifiAdapter extends RecyclerView.Adapter<CanvasWifiAdapter.CanvasViewHolder> {

		@Getter
		@Setter
		List<WifiScanResult> wifiResults;

		public CanvasWifiAdapter(List<WifiScanResult> wifiResults) {
			this.wifiResults = wifiResults;
		}


		@NonNull
		@Override
		public CanvasViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
			View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view_network_wifi, parent, false);
			return new CanvasViewHolder(view);
		}

		@Override
		public void onBindViewHolder(@NonNull CanvasViewHolder holder, int position) {
			final WifiScanResult result = wifiResults.get(holder.getAdapterPosition());

			if (result.rssi >= -40) {
				if (isNoPassword(result.capabilities))
					holder.imageSignalWifi.setImageResource(R.drawable.ic_signal_wifi_4_bar_black_24dp);
				else
					holder.imageSignalWifi.setImageResource(R.drawable.ic_signal_wifi_4_bar_lock_black_24dp);
			} else if (result.rssi >= -55) {
				if (isNoPassword(result.capabilities))
					holder.imageSignalWifi.setImageResource(R.drawable.ic_signal_wifi_3_bar_black_24dp);
				else
					holder.imageSignalWifi.setImageResource(R.drawable.ic_signal_wifi_3_bar_lock_black_24dp);
			} else if (result.rssi >= -70) {
				if (isNoPassword(result.capabilities))
					holder.imageSignalWifi.setImageResource(R.drawable.ic_signal_wifi_2_bar_black_24dp);
				else
					holder.imageSignalWifi.setImageResource(R.drawable.ic_signal_wifi_2_bar_lock_black_24dp);
			} else {
				if (isNoPassword(result.capabilities))
					holder.imageSignalWifi.setImageResource(R.drawable.ic_signal_wifi_1_bar_black_24dp);
				else
					holder.imageSignalWifi.setImageResource(R.drawable.ic_signal_wifi_1_bar_lock_black_24dp);
			}

			holder.txtSSID.setText(result.getSsid());
			holder.rippleItem.setOnClickListener((view) -> {
				Log.w(TAG, "Canvas WIFI == " + result.toString());
				dialogWifiScanResult.dismiss();

				showDialogNetworkConnect(result, view.getContext());
			});
		}

		@Override
		public int getItemCount() {
			return wifiResults.size();
		}


		private boolean isNoPassword(String capabilities) {
			try {
				NetworkType networkType = NetworkType.forIntentValue(capabilities);
				return networkType == NetworkType.NO_PASSWORD;
			} catch (Exception e) {
				return false;
			}
		}




		private EditText passwordInput;

		@SuppressWarnings("ResourceAsColor")
		private void showDialogNetworkConnect(final WifiScanResult ap, final Context context) {
			final View positiveAction;

			final MaterialDialog materialDialog = new MaterialDialog.Builder(context)
					.autoDismiss(false)
					.title(ap.getSsid())
					.titleColorRes(R.color.md_red_500)
					.autoDismiss(false)
					.customView(R.layout.dialog_view_network_connect, true)
					.positiveText(R.string.msg_connect)
					.negativeText(R.string.msg_cancel)
					.onNegative(((dialog, which) -> {
						dialog.dismiss();
					}))
					.onPositive((dialog, which) -> {
						dialog.dismiss();

						final String passwordType = ap.capabilities;
						final String serverAddress = NetworkUtils.intToInetAddress(canvasDhcpInfo.serverAddress).getHostAddress().replace("/", "");
						final String serverPort = "9999";

						final RequestBody requestBody = new FormBody.Builder()
								.addEncoded("ssid", ap.getSsid())
								.addEncoded("password", TextUtils.isEmpty(passwordInput.getText().toString()) ? "" : passwordInput.getText().toString())
								.addEncoded("passwordType", passwordType)
								.addEncoded("auth", CanvasPreference.getInstance(application).getString(CanvasPreference.Keys.Auth))
								.build();
						final Request request = new Request.Builder()
								.url("http://" + serverAddress + ":" + serverPort + "/connect")
								.post(requestBody)
								.build();

						final OkHttpClient client = new OkHttpClient();
						client.newCall(request).enqueue(new Callback() {
							@Override
							public void onFailure(@NonNull Call call, @NonNull IOException e) {
								Log.e(TAG, "Request Error", e);
							}

							@Override
							public void onResponse(@NonNull Call call, @NonNull Response response) throws IOException {
								Log.e(TAG, "success");
								// Log.w(TAG, "response=" + response.toString());

								if (response.code() == 200) {
									WifiConfigManager.configure(wifiManager, ap.getSsid(), passwordInput.getText().toString(), passwordType);
								}


								// TODO
								Intent intent = new Intent();
								setResult(0x1999, intent);

								Handler handler = new Handler(Looper.getMainLooper());
								handler.post(() -> {
									Toast.makeText(application, R.string.msg_request_canvas_connect_done, Toast.LENGTH_SHORT).show();
								});

								handler.postDelayed(NetworkActivity.this::finish, 1000);
							}
						});
					})
					.build();


			positiveAction = materialDialog.getActionButton(DialogAction.POSITIVE);
			passwordInput = materialDialog.getCustomView().findViewById(R.id.password);
			passwordInput.addTextChangedListener(new TextWatcher() {
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				}

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					positiveAction.setEnabled(s.toString().trim().length() > 0);
				}

				@Override
				public void afterTextChanged(Editable s) {
				}
			});
			passwordInput.setText("P@ssw0rd");


			// Toggling the show password CheckBox will mask or unmask the password input EditText
			CheckBox checkbox = materialDialog.getCustomView().findViewById(R.id.showPassword);
			checkbox.setOnCheckedChangeListener(
					(buttonView, isChecked) -> {
						passwordInput.setInputType(
								!isChecked ? InputType.TYPE_TEXT_VARIATION_PASSWORD : InputType.TYPE_CLASS_TEXT);
						passwordInput.setTransformationMethod(
								!isChecked ? PasswordTransformationMethod.getInstance() : null);
					});

			int widgetColor = ThemeSingleton.get().widgetColor;
			MDTintHelper.setTint(checkbox, widgetColor == 0 ? ContextCompat.getColor(materialDialog.getContext(), R.color.colorAccent) : widgetColor);
			MDTintHelper.setTint(passwordInput, widgetColor == 0 ? ContextCompat.getColor(materialDialog.getContext(), R.color.colorAccent) : widgetColor);

			// Position Setup
			Window window = materialDialog.getWindow();
			if (window != null) {
				window.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
				window.setGravity(Gravity.CENTER);
			}
			materialDialog.show();

			positiveAction.setEnabled(false); // disabled by default
		}

		class CanvasViewHolder extends RecyclerView.ViewHolder {
			@BindView(R.id.rippleItem)
			MaterialRippleLayout rippleItem;
			@BindView(R.id.imageSignalWifi)
			ImageView imageSignalWifi;
			@BindView(R.id.txtSSID)
			TextView txtSSID;

			CanvasViewHolder(View itemView) {
				super(itemView);
				ButterKnife.bind(this, itemView);
			}
		}
	}
}
