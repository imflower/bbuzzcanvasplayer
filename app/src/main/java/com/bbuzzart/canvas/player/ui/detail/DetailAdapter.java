package com.bbuzzart.canvas.player.ui.detail;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.bbuzzart.canvas.network.model.Pick;
import com.bbuzzart.canvas.player.R;
import com.bbuzzart.canvas.player.databinding.ItemViewDetailBinding;

import java.util.Collections;
import java.util.List;

public class DetailAdapter extends RecyclerView.Adapter<DetailAdapter.CuratorAdapterViewHolder> {

	private static final String TAG = DetailAdapter.class.getSimpleName();

	private List<Pick> picks;


	public interface AdapterListener {
		void onSelected(Pick pick);
	}

	private AdapterListener adapterListener;

	public void setAdapterListener(AdapterListener adapterListener) {
		this.adapterListener = adapterListener;
	}


	DetailAdapter() {
		this.picks = Collections.emptyList();
	}


	@NonNull
	@Override
	public CuratorAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		final ItemViewDetailBinding itemViewDetailBinding =
				DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
						R.layout.item_view_detail, parent, false);

		return new CuratorAdapterViewHolder(itemViewDetailBinding);
	}

	@Override
	public void onBindViewHolder(@NonNull CuratorAdapterViewHolder holder, int position) {
		holder.bindPick(picks.get(position));
		holder.itemViewDetailBinding.detailCardView.setOnClickListener(v -> {
			long workId = holder.itemViewDetailBinding.getDetailItemViewModel().getPick().getWorkId();
			Log.w(TAG, "workId=" + workId);

			adapterListener.onSelected(picks.get(position));
		});
	}


	@Override
	public int getItemCount() {
		return (null != picks ? picks.size() : 0);
	}

	@Override
	public long getItemId(int position) {
		return picks.get(position).getCurationId();
	}

	void setPicks(List<Pick> picks) {
		this.picks = picks;
		notifyDataSetChanged();
	}


	static class CuratorAdapterViewHolder extends RecyclerView.ViewHolder {
		ItemViewDetailBinding itemViewDetailBinding;

		CuratorAdapterViewHolder(ItemViewDetailBinding itemViewCurationBinding) {
			super(itemViewCurationBinding.detailCardView);
			this.itemViewDetailBinding = itemViewCurationBinding;
		}

		void bindPick(Pick pick) {
			itemViewDetailBinding.setDetailItemViewModel(new DetailItemViewModel(pick));
			// itemViewDetailBinding.notifyChange();
		}
	}

}
