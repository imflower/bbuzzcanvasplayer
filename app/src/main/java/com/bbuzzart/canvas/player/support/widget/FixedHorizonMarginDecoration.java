package com.bbuzzart.canvas.player.support.widget;

/**
 * Created by imflower on 27/03/2018.
 */

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.bbuzzart.canvas.player.R;

public class FixedHorizonMarginDecoration extends RecyclerView.ItemDecoration {

	private static final int fixedVal = 20;

	@Override
	public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
		int margin = parent.getResources().getDimensionPixelSize(R.dimen.item_margin);

		outRect.left = margin;
		outRect.right = 0;
		outRect.top = 0;
		outRect.bottom = 0;

		if (parent.getChildAdapterPosition(view) == 0) {
			outRect.left = 0;
		} else {
			outRect.left = margin;
		}
	}
}