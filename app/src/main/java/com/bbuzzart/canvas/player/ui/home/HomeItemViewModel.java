package com.bbuzzart.canvas.player.ui.home;

import android.databinding.BaseObservable;
import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.bbuzzart.canvas.network.model.Curations;
import com.bbuzzart.canvas.player.CanvasPreference;
import com.bbuzzart.canvas.player.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import jp.wasabeef.glide.transformations.BlurTransformation;
import lombok.Getter;

public class HomeItemViewModel extends BaseObservable {

	public static final String TAG = HomeItemViewModel.class.getSimpleName();

	@Getter
	private Curations.Data.Curation curation;


	HomeItemViewModel(Curations.Data.Curation curation) {
		this.curation = curation;
	}

	public String getTitle() {
		return curation.getCurationTitle();
	}

	public String getCurator() {
		return curation.getCuratorName();
	}

	public String getComment() {
		return curation.getCurationComment();
	}

	public String getImageProfile() {
		return curation.getCuratorProfileImgsUrl();
	}

	public String getImageUrl() {
		return curation.getPick().getWorkImgsUrl();
	}


	@BindingAdapter("imageUrl")
	public static void setImageUrl(ImageView imageView, String url) {
		String cdnDomain = CanvasPreference.getInstance(imageView.getContext())
				.getString(CanvasPreference.Keys.CdnDomain);

		Glide.with(imageView.getContext())
				.load(cdnDomain + url)
				.apply(RequestOptions
						.bitmapTransform(new BlurTransformation(25))
						.placeholder(R.color.md_grey_100)
						.error(R.color.md_white_1000))
				.into(imageView);
	}

	@BindingAdapter("imageProfile")
	public static void setImageProfile(ImageView imageView, String url) {
		String cdnDomain = CanvasPreference.getInstance(imageView.getContext())
				.getString(CanvasPreference.Keys.CdnDomain);

		if (url != null) {
			Glide.with(imageView.getContext())
					.load(cdnDomain + url)
					.apply(new RequestOptions()
							.circleCrop()
							.placeholder(R.drawable.temp_profile_thumb)
							.error(R.drawable.temp_profile_thumb))
					.into(imageView);
		}
	}

	public void setCuration(Curations.Data.Curation curation) {
		this.curation = curation;
		notifyChange();
	}

}
