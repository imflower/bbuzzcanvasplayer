package com.bbuzzart.canvas.player;

import android.app.Application;
import android.content.Context;

import com.bbuzzart.canvas.network.model.Account;
import com.bbuzzart.canvas.network.model.Initialize;
import com.bbuzzart.canvas.network.service.BuzzArtFactory;
import com.bbuzzart.canvas.network.service.BuzzArtService;

import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;
import lombok.Getter;
import lombok.Setter;

public class CanvasApplication extends Application {

	private Scheduler scheduler;

	private BuzzArtService buzzArtService;

	@Getter
	@Setter
	private Account account;

	@Getter
	@Setter
	private Initialize buzzArtServerConfig;

	private static CanvasApplication get(Context context) {
		return (CanvasApplication) context.getApplicationContext();
	}

	public static CanvasApplication create(Context context) {
		return CanvasApplication.get(context);
	}

	public Scheduler subscribeScheduler() {
		if (scheduler == null) {
			scheduler = Schedulers.io();
		}

		return scheduler;
	}


	public BuzzArtService getBuzzArtService() {
		if (buzzArtService == null) {
			buzzArtService = BuzzArtFactory.create();
		}

		return buzzArtService;
	}
}
