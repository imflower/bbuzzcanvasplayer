package com.bbuzzart.canvas.player.ui.home;

import android.content.Context;
import android.databinding.ObservableInt;
import android.support.annotation.NonNull;
import android.util.Log;

import com.bbuzzart.canvas.network.model.Curations;
import com.bbuzzart.canvas.network.service.BuzzArtService;
import com.bbuzzart.canvas.player.CanvasApplication;

import java.util.Observable;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import lombok.Getter;

public class HomeViewModel extends Observable {

	private static final String TAG = HomeViewModel.class.getSimpleName();

	public ObservableInt recyclerView;

	private Context context;

	@Getter
	private Curations curations;

	private CompositeDisposable compositeDisposable = new CompositeDisposable();

	public HomeViewModel(@NonNull Context context) {
		this.context = context;
		this.curations = new Curations();
	}

	public void fetchCurations() {
		CanvasApplication application = CanvasApplication.create(context);
		BuzzArtService service = application.getBuzzArtService();

		Disposable disposable = service.fetchCurations(null, true, "curatorpick", 1, 10)
				.subscribeOn(application.subscribeScheduler())
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(
						curations -> {
							if (curations.getStatus()) {
								Log.w(TAG, curations.getData().getPagination().toString());

								// TODO success
								this.curations.getData().setPagination(curations.getData().getPagination());
								this.curations.getData().getCurations().clear();
								this.curations.getData().getCurations().addAll(curations.getData().getCurations());

								setChanged();
								notifyObservers();
							} else {
								// TODO fail
								Log.e("Model", curations.toString());
							}
						},
						throwable -> {
							Log.e("Model", "Fetch Error", throwable);
						}
				);
		compositeDisposable.add(disposable);
	}


	public void reset() {
		if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
			compositeDisposable.dispose();
		}

		compositeDisposable = null;
		context = null;
	}
}
