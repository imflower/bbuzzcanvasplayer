package com.bbuzzart.canvas.player.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.bbuzzart.canvas.network.model.Curations;
import com.bbuzzart.canvas.player.CanvasApplication;
import com.bbuzzart.canvas.player.CanvasPreference;
import com.bbuzzart.canvas.player.R;
import com.bbuzzart.canvas.player.support.application.BaseActivity;
import com.bbuzzart.canvas.player.ui.detail.DetailFragment;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import org.parceler.Parcels;

import butterknife.BindView;
import butterknife.OnClick;

public class DetailActivity extends BaseActivity {

	private static final String TAG = DetailActivity.class.getSimpleName();

	private CanvasApplication application;
	private Curations.Data.Curation curation;
	private FragmentManager fragmentManager;

	@BindView(R.id.toolbar)
	Toolbar toolbar;

	@BindView(R.id.imageProfile)
	ImageView imageProfile;
	@BindView(R.id.txtCurator)
	TextView txtCurator;
	@BindView(R.id.txtTitle)
	TextView txtTitle;
	@BindView(R.id.txtCurationTitle)
	TextView txtCurationTitle;
	@BindView(R.id.txtComment)
	TextView txtComment;


	@OnClick(R.id.ripplePlayCuration)
	public void ripplePlayCuration() {
		Intent intent = new Intent(application, DetailPlayActivity.class);
		intent.putExtra("curation", Parcels.wrap(curation));
		startActivity(intent);
	}



	@Override
	protected int getLayoutResource() {
		return R.layout.activity_detail;
	}

	@Override
	protected void initVariables(Bundle savedInstanceState) {
		this.application = (CanvasApplication) getApplicationContext();
		this.fragmentManager = getSupportFragmentManager();

		if (getIntent() != null) {
			this.curation = Parcels.unwrap(getIntent().getParcelableExtra("curation"));
			// Log.w(TAG, "this.curation===" + this.curation.toString());


			String cdnDomain = CanvasPreference.getInstance(application)
					.getString(CanvasPreference.Keys.CdnDomain);

			Glide.with(application)
					.load(cdnDomain + curation.getCuratorProfileImgsUrl())
					.apply(new RequestOptions()
							.circleCrop()
							.placeholder(R.drawable.temp_profile_thumb)
							.error(R.drawable.temp_profile_thumb))
					.into(imageProfile);


			txtCurator.setText("By " + curation.getCuratorName());
			txtCurationTitle.setText(curation.getCurationTitle());
			txtComment.setText(curation.getCurationComment());
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				onBackPressed();
				return true;
		}
		return false;
	}

	@Override
	public void onBackPressed() {
		finish();
	}


	@Override
	protected void initData(Bundle savedInstanceState) {
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowTitleEnabled(false);

		if (this.curation != null)
			setFragment();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}


	private void setFragment() {
		DetailFragment fragment = DetailFragment.newInstance(curation);

		FragmentTransaction transaction = fragmentManager.beginTransaction();
		transaction.replace(R.id.detailFragment, fragment, DetailFragment.TAG);
		transaction.addToBackStack(DetailFragment.TAG);
		transaction.commit();
	}


}
