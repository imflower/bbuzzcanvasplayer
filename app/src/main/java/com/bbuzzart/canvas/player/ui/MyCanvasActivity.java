package com.bbuzzart.canvas.player.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;
import com.bbuzzart.canvas.network.model.Canvas;
import com.bbuzzart.canvas.network.service.BuzzArtService;
import com.bbuzzart.canvas.player.CanvasApplication;
import com.bbuzzart.canvas.player.R;
import com.bbuzzart.canvas.player.support.application.BaseActivity;

import org.parceler.Parcels;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import lombok.Getter;
import lombok.Setter;

public class MyCanvasActivity extends BaseActivity {

	public static final String TAG = MyCanvasActivity.class.getSimpleName();

	private CanvasApplication application;
	private CompositeDisposable compositeDisposable;


	@BindView(R.id.rootView)
	RelativeLayout rootView;
	@BindView(R.id.toolbar)
	Toolbar toolbar;
	@BindView(R.id.txtTitle)
	TextView txtTitle;

	@BindView(R.id.txtTotalCanvas)
	TextView txtTotalCanvas;
	@BindView(R.id.recyclerViewCanvas)
	RecyclerView recyclerViewCanvas;
	@BindView(R.id.recyclerViewEmpty)
	LinearLayout recyclerViewEmpty;
	@BindView(R.id.progressView)
	LinearLayout progressView;


	@OnClick(R.id.rippleRefresh)
	public void rippleRefresh() {
		fetchCanvases();
	}


	@Override
	protected int getLayoutResource() {
		return R.layout.activity_canvas_mine;
	}

	@Override
	protected void initVariables(Bundle savedInstanceState) {
		this.application = (CanvasApplication) getApplicationContext();
		compositeDisposable = new CompositeDisposable();
	}

	@Override
	protected void initData(Bundle savedInstanceState) {
		setSupportActionBar(toolbar);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowTitleEnabled(false);

		txtTitle.setText(getString(R.string.msg_canvas_info_mine));

		fetchCanvases();
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				onBackPressed();
				return true;
		}
		return false;
	}

	@Override
	public void onBackPressed() {
		finish();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (resultCode == 0x1234) {
			fetchCanvases();
		}
	}

	private void fetchCanvases() {
		progressView.setVisibility(View.VISIBLE);

		String auth = application.getAccount().getAuth();
		BuzzArtService service = application.getBuzzArtService();

		Disposable disposable = service.fetchCanvases(auth, true, 1, 10)
				.subscribeOn(application.subscribeScheduler())
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(
						result -> {
							Log.w(TAG, "fetchCanvases Result");
							Log.w(TAG, result.toString());
							if (result.getStatus()) {
								// SUCCESS
								txtTotalCanvas.setText("" + result.getData().getCanvases().size());

								if (result.getData().getCanvases().size() > 0) {
									CanvasAdapter canvasAdapter = new CanvasAdapter(application, result.getData().getCanvases());
									recyclerViewCanvas.setAdapter(canvasAdapter);
									recyclerViewCanvas.setLayoutManager(new LinearLayoutManager(MyCanvasActivity.this));
									recyclerViewCanvas.setFocusable(false);
									recyclerViewCanvas.setVisibility(View.VISIBLE);
									recyclerViewEmpty.setVisibility(View.GONE);
								} else {
									recyclerViewCanvas.setVisibility(View.GONE);
									recyclerViewEmpty.setVisibility(View.VISIBLE);
								}
							} else {
								Log.w(TAG, result.toString());
							}

							progressView.setVisibility(View.GONE);
						},
						throwable -> {
							Log.e(TAG, throwable.toString());
							progressView.setVisibility(View.GONE);
						}
				);

		compositeDisposable.add(disposable);
	}


	class CanvasAdapter extends RecyclerView.Adapter<MyCanvasActivity.CanvasAdapter.CanvasViewHolder> {

		@Getter
		@Setter
		List<Canvas> canvases;

		Context context;

		public CanvasAdapter(Context context, List<Canvas> canvases) {
			this.context = context;
			this.canvases = canvases;
		}

		@NonNull
		@Override
		public CanvasAdapter.CanvasViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
			View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view_my_canvas, parent, false);
			return new CanvasAdapter.CanvasViewHolder(view);
		}

		@Override
		public void onBindViewHolder(@NonNull CanvasAdapter.CanvasViewHolder holder, int position) {
			final Canvas canvas = canvases.get(holder.getAdapterPosition());

			holder.txtCanvasName.setText(canvas.getCanvasName());

			if (canvas.getCanvasSetting().getData().getType().equals("curation")) {
				holder.txtCanvasCurationType.setText(getString(R.string.msg_canvas_curation_type_curation));
			} else {
				holder.txtCanvasCurationType.setText(getString(R.string.msg_canvas_curation_type_work));
			}
			holder.txtCanvasCurationName.setText(canvas.getCanvasSetting().getData().getTitle());

			if (canvas.getIsConnect()) {
				holder.txtCanvasConnectState.setText(R.string.msg_canvas_connect_state_ok);
				holder.txtCanvasConnectState.setTextColor(getResources().getColor(R.color.md_blue_700));
			} else {
				holder.txtCanvasConnectState.setText(R.string.msg_canvas_connect_state_disconnect);
				holder.txtCanvasConnectState.setTextColor(getResources().getColor(R.color.md_red_500));
			}

			holder.rippleItem.setOnClickListener((view) -> {
				Intent intent = new Intent(application, SettingActivity.class);
				intent.putExtra("canvas", Parcels.wrap(canvas));
				startActivityForResult(intent, 0x1234);
			});
		}

		@Override
		public int getItemCount() {
			return canvases.size();
		}

		@Override
		public long getItemId(int position) {
			return canvases.get(position).getCanvasId();
		}

		class CanvasViewHolder extends RecyclerView.ViewHolder {
			@BindView(R.id.rippleItem)
			MaterialRippleLayout rippleItem;
			@BindView(R.id.txtCanvasName)
			TextView txtCanvasName;
			@BindView(R.id.txtCanvasCurationType)
			TextView txtCanvasCurationType;
			@BindView(R.id.txtCanvasCurationName)
			TextView txtCanvasCurationName;
			@BindView(R.id.txtCanvasConnectState)
			TextView txtCanvasConnectState;

			CanvasViewHolder(View itemView) {
				super(itemView);
				ButterKnife.bind(this, itemView);
			}
		}
	}

}
