package com.bbuzzart.canvas.player.ui;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bbuzzart.canvas.network.model.Account;
import com.bbuzzart.canvas.network.service.BuzzArtService;
import com.bbuzzart.canvas.player.CanvasApplication;
import com.bbuzzart.canvas.player.CanvasPreference;
import com.bbuzzart.canvas.player.R;
import com.bbuzzart.canvas.player.support.application.BaseActivity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.github.javiersantos.materialstyleddialogs.MaterialStyledDialogWithDesc;
import com.github.javiersantos.materialstyleddialogs.enums.Style;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.holder.ImageHolder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SectionDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;
import com.mikepenz.materialdrawer.util.AbstractDrawerImageLoader;
import com.mikepenz.materialdrawer.util.DrawerImageLoader;
import com.mikepenz.materialdrawer.util.DrawerUIUtils;
import com.mikepenz.materialize.util.UIUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;


public class HomeActivity extends BaseActivity {

	private static final String TAG = HomeActivity.class.getSimpleName();

	private static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 5;

	private CanvasApplication application;
	private Drawer result = null;

	@BindView(R.id.rootView)
	RelativeLayout rootView;
	@BindView(R.id.toolbar)
	Toolbar toolbar;


	@Override
	protected int getLayoutResource() {
		return R.layout.activity_home;
	}

	@Override
	protected void initVariables(Bundle savedInstanceState) {
		this.application = (CanvasApplication) getApplicationContext();
	}

	@Override
	protected void initData(Bundle savedInstanceState) {
		setSupportActionBar(toolbar);
		checkAndRequestPermissions();
		createDrawer(savedInstanceState);


		/*
		try {
			CanvasPreference.getInstance(getApplicationContext()).putString(CanvasPreference.Keys.Auth, "");
		} catch (Exception e) {
			e.printStackTrace();
		}
		*/
	}


	private static final long FINISH_INTERVAL_TIME = 1000L;
	private long backPressedTime;

	@Override
	public void onBackPressed() {
		if (result != null && result.isDrawerOpen()) {
			result.closeDrawer();
			return;
		}

		long tempTime = System.currentTimeMillis();
		long intervalTime = tempTime - backPressedTime;
		if (0 <= intervalTime && FINISH_INTERVAL_TIME >= intervalTime) {
			finish();
		} else {
			backPressedTime = tempTime;
			Toast.makeText(getApplicationContext(), getString(R.string.app_finish), Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}


	private void createDrawer(Bundle savedInstanceState) {
		final String profileImage =
				application.getBuzzArtServerConfig().getData().getCdn().getDomain() +
						application.getAccount().getProfileImgsUrl();

		final String profileBackground =
				application.getBuzzArtServerConfig().getData().getCdn().getDomain() +
						application.getAccount().getCoverImgUrl();

		Log.d(TAG, "profileImage=" + profileImage);

		//initialize and create the image loader logic
		DrawerImageLoader.init(new AbstractDrawerImageLoader() {
			@Override
			public void set(ImageView imageView, Uri uri, Drawable placeholder) {
				Glide.with(imageView.getContext())
						.load(uri)
						.apply(new RequestOptions().placeholder(placeholder))
						.into(imageView);
			}

			@Override
			public void cancel(ImageView imageView) {
				Glide.with(imageView.getContext()).clear(imageView);
			}

			@Override
			public Drawable placeholder(Context ctx, String tag) {
				if (DrawerImageLoader.Tags.PROFILE.name().equals(tag)) {
					return DrawerUIUtils.getPlaceHolder(ctx);
				} else if (DrawerImageLoader.Tags.ACCOUNT_HEADER.name().equals(tag)) {
					return new IconicsDrawable(ctx).iconText(" ").backgroundColorRes(R.color.colorPrimary).sizeDp(56);
				} else if ("customUrlItem".equals(tag)) {
					return new IconicsDrawable(ctx).iconText(" ").backgroundColorRes(R.color.colorAccent).sizeDp(56);
				}

				return super.placeholder(ctx, tag);
			}
		});

		final IProfile profile = new ProfileDrawerItem()
				.withIcon(profileImage)
				.withEmail(application.getAccount().getEmail())
				.withName(application.getAccount().getName());

		AccountHeader headerResult = new AccountHeaderBuilder()
				.withActivity(this)
				.withCompactStyle(false)
				.withHeaderBackground(new ImageHolder(profileBackground))
				.withSavedInstance(savedInstanceState)
				.addProfiles(profile)
				.withSelectionListEnabledForSingleProfile(false)
				.withProfileImagesVisible(true)
				.build();

		//Create the drawer
		result = new DrawerBuilder()
				.withActivity(this)
				.withToolbar(toolbar)
				.withAccountHeader(headerResult)
				.withActionBarDrawerToggle(true)
				.withActionBarDrawerToggleAnimated(true)
				.addDrawerItems(
						new SectionDrawerItem().withName(R.string.msg_user_info).withDivider(false),
						new PrimaryDrawerItem().withName(R.string.msg_user_info_change).withIcon(R.drawable.ic_change_account).withIdentifier(1),
						new PrimaryDrawerItem().withName(R.string.msg_user_info_password).withIcon(R.drawable.ic_change_password).withIdentifier(2),
						new PrimaryDrawerItem().withName(R.string.msg_user_info_logout).withIcon(R.drawable.ic_logout).withIdentifier(3),
						new SectionDrawerItem().withName(R.string.msg_canvas_info).withDivider(false),
						new PrimaryDrawerItem().withName(R.string.msg_canvas_info_new).withIcon(R.drawable.ic_new_canvas).withIdentifier(11),
						new PrimaryDrawerItem().withName(R.string.msg_canvas_info_mine).withIcon(R.drawable.ic_my_canvas).withIdentifier(12),
						new SectionDrawerItem().withName(R.string.msg_cs_info).withDivider(false),
						new PrimaryDrawerItem().withName(R.string.msg_canvas_info_notice).withIcon(R.drawable.ic_info).withIdentifier(21),
						new PrimaryDrawerItem().withName(R.string.msg_canvas_info_faq).withIcon(R.drawable.ic_q_a).withIdentifier(22),
						new PrimaryDrawerItem().withName(R.string.msg_canvas_info_contact).withIcon(R.drawable.ic_contact).withIdentifier(23)
				)
				.withOnDrawerItemClickListener((view, position, drawerItem) -> {
					if (drawerItem != null && drawerItem.getIdentifier() == 1) {
						startSupportActionMode(new ActionBarCallBack());
						findViewById(R.id.action_mode_bar).setBackgroundColor(
								UIUtils.getThemeColorFromAttrOrRes(HomeActivity.this,
										R.attr.colorPrimary, R.color.material_drawer_primary));
					}

					if (drawerItem != null) {
						switch ((int) drawerItem.getIdentifier()) {
							case 1:
								startActivity(new Intent(Intent.ACTION_VIEW,
										Uri.parse("https://www.bbuzzart.com/profile/" +
										application.getAccount().getUsrId())).setPackage("com.android.chrome"));
								result.closeDrawer();
								break;
							case 2:
								startActivity(new Intent(Intent.ACTION_VIEW,
										Uri.parse("https://www.bbuzzart.com/account/change-password")).setPackage("com.android.chrome"));
								result.closeDrawer();
								break;
							case 3:
								doPostLogout();
								break;
							case 11:
								startActivity(new Intent(application, NetworkActivity.class));
								break;
							case 12:
								startActivity(new Intent(application, MyCanvasActivity.class));
								break;
							case 21:
								startActivity(new Intent(Intent.ACTION_VIEW,
										Uri.parse("https://www.bbuzzart.com/support/notice")));
								break;
							case 22:
								startActivity(new Intent(Intent.ACTION_VIEW,
										Uri.parse("https://www.bbuzzart.com/support/help")));
								break;
							case 23:
								startActivity(new Intent(Intent.ACTION_VIEW,
										Uri.parse("https://www.bbuzzart.com/support/contactus")));
								break;
						}

					}

					return false;
				})
				.withSavedInstance(savedInstanceState)
				.build();

		// set the selection to the item with the identifier 5
		if (savedInstanceState == null) {
			result.setSelection(-1);
		}

		getSupportActionBar().setDisplayHomeAsUpEnabled(false);
		getSupportActionBar().setDisplayShowTitleEnabled(false);

		result.getActionBarDrawerToggle().setDrawerSlideAnimationEnabled(true);
		result.getActionBarDrawerToggle().setDrawerIndicatorEnabled(true);
	}


	class ActionBarCallBack implements ActionMode.Callback {

		@Override
		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
			return false;
		}

		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
				getWindow().setStatusBarColor(
						UIUtils.getThemeColorFromAttrOrRes(HomeActivity.this,
								R.attr.colorPrimaryDark, R.color.material_drawer_primary_dark));
			}

			// mode.getMenuInflater().inflate(R.menu.cab, menu);
			return true;
		}

		@Override
		public void onDestroyActionMode(ActionMode mode) {
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
				getWindow().setStatusBarColor(Color.TRANSPARENT);
			}
		}

		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			return false;
		}
	}


	@TargetApi(Build.VERSION_CODES.M)
	private boolean checkAndRequestPermissions() {
		int permissionSendMessage = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);

		List<String> listPermissionsNeeded = new ArrayList<>();
		if (permissionSendMessage != PackageManager.PERMISSION_GRANTED) {
			listPermissionsNeeded.add(Manifest.permission.ACCESS_COARSE_LOCATION);
			listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
		}

		if (!listPermissionsNeeded.isEmpty()) {
			ActivityCompat.requestPermissions(this,
					listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]),
					REQUEST_ID_MULTIPLE_PERMISSIONS
			);
			return false;
		}

		return true;
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
		switch (requestCode) {
			case REQUEST_ID_MULTIPLE_PERMISSIONS: {
				Map<String, Integer> perms = new HashMap<>();
				perms.put(Manifest.permission.ACCESS_COARSE_LOCATION, PackageManager.PERMISSION_GRANTED);

				if (grantResults.length > 0) {
					for (int i = 0; i < permissions.length; i++)
						perms.put(permissions[i], grantResults[i]);

					// Check for both permissions
					if (perms.get(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
						Log.d(TAG, "sms & location services permission granted");

						// TODO
					} else {
						Log.d(TAG, "Some permissions are not granted ask again ");

						final MaterialStyledDialogWithDesc permissionDialog = new MaterialStyledDialogWithDesc(HomeActivity.this)
								.setStyle(Style.HEADER_WITH_FUNBIT)
								.setHeaderColor(R.color.colorPrimary)
								.setTitle("Permission required")
								.setDescription("Location Permission required")
								.withDialogAnimation(true)
								.withIconAnimation(false)
								.withDivider(true)
								.setCancelable(false)
								.setNegative(getString(R.string.msg_cancel), (dialog, which) -> dialog.dismiss())
								.setPositive(getString(R.string.msg_ok), (dialog, which) -> {
									dialog.dismiss();
									checkAndRequestPermissions();
								});
						permissionDialog.show();
					}
				}
			}
		}
	}


	private void doPostLogout() {
		try {
			application.setAccount(new Account());
			CanvasPreference.getInstance(application).putString(CanvasPreference.Keys.Auth, null);
			finish();
		} catch (Exception e) {
			Log.e(TAG, "doLogout", e);
		}
	}
}
