package com.bbuzzart.canvas.player.ui.detail;

import android.databinding.BaseObservable;
import android.databinding.BindingAdapter;
import android.util.Log;
import android.widget.ImageView;

import com.bbuzzart.canvas.network.model.Pick;
import com.bbuzzart.canvas.player.CanvasPreference;
import com.bbuzzart.canvas.player.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import jp.wasabeef.glide.transformations.BlurTransformation;
import lombok.Getter;

public class DetailItemViewModel extends BaseObservable {

	public static final String TAG = DetailItemViewModel.class.getSimpleName();

	@Getter
	private Pick pick;


	DetailItemViewModel(Pick pick) {
		setPick(pick);
	}

	public String getTitle() {
		return pick.getWorkTitle();
	}

	public String getImageWork() {
		return pick.getWorkImgsUrl();
	}


	@BindingAdapter("imageWork")
	public static void setImageWork(ImageView imageView, String url) {
		String cdnDomain = CanvasPreference.getInstance(imageView.getContext())
				.getString(CanvasPreference.Keys.CdnDomain);
		final String imageUrl = cdnDomain + url;

		Log.w(TAG, "workUrl=" + imageUrl);

		Glide.with(imageView.getContext())
				.load(imageUrl)
				.apply(new RequestOptions()
						.placeholder(R.color.md_grey_200)
						.error(R.color.md_white_1000))
				.into(imageView);
	}

	private void setPick(Pick pick) {
		this.pick = pick;
		notifyChange();
	}

}
