package com.bbuzzart.canvas.player;

/**
 * Created by imflower on 13/02/2018.
 */

public interface CanvasConstant {

	static final String REMOTE_API_SERVER_URI = "https://dev2-api.bbuzzart.com/v4/";
	static final String REMOTE_IMAGE_BASE_URI = "https://djo93u9c0domr.cloudfront.net";


	static final String ACTION_NETWORK_SERVICE_START = "com.buzzart.canvas.network.start";

}
