package com.bbuzzart.canvas.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@Parcel(Parcel.Serialization.BEAN)
public class User {

	@SerializedName("usrId")
	@Expose
	public int usrId;
	@SerializedName("email")
	@Expose
	public String email;
	@SerializedName("name")
	@Expose
	public String name;
	@SerializedName("profileImgUrl")
	@Expose
	public String profileImgUrl;
	@SerializedName("profileImgsUrl")
	@Expose
	public String profileImgsUrl;
	@SerializedName("profileImgmUrl")
	@Expose
	public String profileImgmUrl;
	@SerializedName("thumbnailImgUrl")
	@Expose
	public String thumbnailImgUrl;
	@SerializedName("coverImgUrl")
	@Expose
	public String coverImgUrl;
	@SerializedName("coverImgsUrl")
	@Expose
	public String coverImgsUrl;
	@SerializedName("coverImgmUrl")
	@Expose
	public String coverImgmUrl;
	@SerializedName("description")
	@Expose
	public String description;
	@SerializedName("education")
	@Expose
	public String education;
	@SerializedName("urls")
	@Expose
	public String urls;
	@SerializedName("followingCount")
	@Expose
	public Object followingCount;
	@SerializedName("followerCount")
	@Expose
	public Object followerCount;
	@SerializedName("workCount")
	@Expose
	public Object workCount;
	@SerializedName("createDate")
	@Expose
	public String createDate;
	@SerializedName("rcmdCode")
	@Expose
	public Object rcmdCode;
	@SerializedName("rcmdUsrId")
	@Expose
	public Object rcmdUsrId;
	@SerializedName("ctcId")
	@Expose
	public Object ctcId;
	@SerializedName("artEmail")
	@Expose
	public Object artEmail;
	@SerializedName("areaCode")
	@Expose
	public Object areaCode;
	@SerializedName("tel")
	@Expose
	public Object tel;
	@SerializedName("addr")
	@Expose
	public Object addr;
	@SerializedName("addrDetail")
	@Expose
	public Object addrDetail;
	@SerializedName("country")
	@Expose
	public Object country;
	@SerializedName("city")
	@Expose
	public Object city;
	@SerializedName("region")
	@Expose
	public Object region;
	@SerializedName("legalName")
	@Expose
	public Object legalName;
	@SerializedName("zipCode")
	@Expose
	public Object zipCode;
	@SerializedName("paypalEmail")
	@Expose
	public Object paypalEmail;
	@SerializedName("accName")
	@Expose
	public Object accName;
	@SerializedName("accNumber")
	@Expose
	public Object accNumber;
	@SerializedName("bankName")
	@Expose
	public Object bankName;
	@SerializedName("bankRoutingNumber")
	@Expose
	public Object bankRoutingNumber;
	@SerializedName("isFollowing")
	@Expose
	public boolean isFollowing;
	@SerializedName("isArtEmailConfirm")
	@Expose
	public boolean isArtEmailConfirm;
	@SerializedName("isTelConfirm")
	@Expose
	public boolean isTelConfirm;
	@SerializedName("isIdentityConfirm")
	@Expose
	public boolean isIdentityConfirm;
	@SerializedName("artistTypes")
	@Expose
	public Object artistTypes;
	@SerializedName("works")
	@Expose
	public Object works;
	@SerializedName("userAuthority")
	@Expose
	public UserAuthority userAuthority;


	@Getter
	@ToString
	@Parcel(Parcel.Serialization.BEAN)
	public static class UserAuthority {

		@SerializedName("master")
		@Expose
		public boolean master;
		@SerializedName("admin")
		@Expose
		public boolean admin;
		@SerializedName("curator")
		@Expose
		public boolean curator;
		@SerializedName("artist")
		@Expose
		public boolean artist;
		@SerializedName("user")
		@Expose
		public boolean user;
		@SerializedName("anoymous")
		@Expose
		public boolean anoymous;
		@SerializedName("blacklist")
		@Expose
		public boolean blacklist;

	}
}

