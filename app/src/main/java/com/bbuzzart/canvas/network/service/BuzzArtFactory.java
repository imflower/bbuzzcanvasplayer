/*
 * Created by imDangerous on 13/02/2018.
 */

package com.bbuzzart.canvas.network.service;


import com.bbuzzart.canvas.player.CanvasConstant;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class BuzzArtFactory implements CanvasConstant {

	public static BuzzArtService create() {
		OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
		httpClient.addInterceptor(
				chain -> {
					Request original = chain.request();

					Request request = original.newBuilder()
							.header("Accept", "application/json; charset=UTF-8")
							.header("Content-Type", "application/json")
							.header("Application", "e4fe5b8b9a62ccb63e3e5343dc928b8f83263efe230109b645470ba477706265")
							.header("device", "android")
							.header("AppVersion", "1.0.0")
							.method(original.method(), original.body())
							.build();

					return chain.proceed(request);
				});

		OkHttpClient client = httpClient.build();

		Retrofit retrofit = new Retrofit.Builder().baseUrl(REMOTE_API_SERVER_URI)
				.addConverterFactory(GsonConverterFactory.create())
				.addCallAdapterFactory(RxJava2CallAdapterFactory.create())
				.client(client)
				.build();
		return retrofit.create(BuzzArtService.class);
	}
}
