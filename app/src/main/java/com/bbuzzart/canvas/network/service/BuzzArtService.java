package com.bbuzzart.canvas.network.service;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.bbuzzart.canvas.network.model.Canvas;
import com.bbuzzart.canvas.network.model.CanvasApply;
import com.bbuzzart.canvas.network.model.Canvases;
import com.bbuzzart.canvas.network.model.Curations;
import com.bbuzzart.canvas.network.model.CuratorPicks;
import com.bbuzzart.canvas.network.model.Initialize;
import com.bbuzzart.canvas.network.model.SignAccount;
import com.bbuzzart.canvas.network.model.Success;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface BuzzArtService {

	@GET("initialize")
	Observable<Initialize> fetchInitialize();

	@GET("canvases")
	Observable<Canvases> fetchCanvases(
			@Header("Authorization") @Nullable String authorization,
			@Query("all") @NonNull Boolean all,
			@Query("page") @Nullable Integer page,
			@Query("size") @Nullable Integer size
	);

	@POST("canvases/{canvasId}")
	Observable<Canvases> doPostCanvasesModify(
			@Header("Authorization") @Nullable String authorization,
			@Path("canvasId") @NonNull Integer canvasId,
			@Body Canvas canvas
	);

	@POST("canvases/apply")
	Observable<Success> doPostCanvasesApply(
			@Header("Authorization") @Nullable String authorization,
			@Body CanvasApply canvasDataForm
	);

	@POST("canvases/{canvasId}/release")
	Observable<Success> doPostCanvasesRelease(
			@Header("Authorization") @Nullable String authorization,
			@Path("canvasId") @NonNull Integer canvasId
	);

	@GET("curations")
	Observable<Curations> fetchCurations(
			@Header("Authorization") @Nullable String authorization,
			@Query("all") @Nullable Boolean all,
			@Query("type") @Nullable String type,
			@Query("page") @Nullable Integer page,
			@Query("size") @Nullable Integer size
	);

	@GET("curations/{curationId}/picks")
	Observable<CuratorPicks> fetchCuratorPicks(
			@Path("curationId") @NonNull Integer curationId,
			@Header("Authorization") @Nullable String authorization,
			@Query("all") @Nullable Boolean all,
			@Query("page") @Nullable Integer page,
			@Query("size") @Nullable Integer size
	);

	@POST("account/login-auth")
	Observable<SignAccount> doPostAuthSign(
			@Header("Authorization") @NonNull String authorization
	);

	@POST("account/login-email")
	Observable<SignAccount> doPostEmailSign(
			@Query("email") @NonNull String email,
			@Query("password") @NonNull String password
	);

	@POST("account/login-google")
	Observable<SignAccount> doPostGoogleSign(
			@Query("gEmail") @NonNull String email,
			@Query("gUsrName") @NonNull String usrName,
			@Query("gClientId") @NonNull String clientId,
			@Query("gAccessToken") @NonNull String accessToken
	);

	@POST("account/login-facebook")
	Observable<SignAccount> doPostFacebookSign(
			@Query("fbEmail") @NonNull String email,
			@Query("fbUsrName") @NonNull String usrName,
			@Query("fbClientId") @NonNull String clientId,
			@Query("fbAccessToken") @NonNull String accessToken
	);

}
