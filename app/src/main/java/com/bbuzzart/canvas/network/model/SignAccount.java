package com.bbuzzart.canvas.network.model;

import com.bbuzzart.canvas.network.base.ServerResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Created by imflower on 21/02/2018.
 */

@Parcel(value = Parcel.Serialization.BEAN, analyze = SignAccount.class)
@Getter
@Setter
@ToString
public class SignAccount extends ServerResponse {

	@SerializedName("data")
	@Expose
	public Data data;

	@Getter
	@Setter
	@ToString
	@Parcel(value = Parcel.Serialization.BEAN, analyze = Data.class)
	public static class Data {

		@Setter
		@Getter
		@SerializedName("account")
		@Expose
		public Account account;

	}

}
