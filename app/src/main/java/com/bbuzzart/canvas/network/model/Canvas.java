package com.bbuzzart.canvas.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Parcel(value = Parcel.Serialization.BEAN, analyze = Canvas.class)
public class Canvas {

	@SerializedName("canvasId")
	@Expose
	public Integer canvasId;
	@SerializedName("canvasDisplayId")
	@Expose
	public String canvasDisplayId;
	@SerializedName("canvasName")
	@Expose
	public String canvasName;
	@SerializedName("canvasUid")
	@Expose
	public String canvasUid;
	@SerializedName("canvasSetting")
	@Expose
	public CanvasSetting canvasSetting;
	@SerializedName("wifiName")
	@Expose
	public String wifiName;
	@SerializedName("isConnect")
	@Expose
	public Boolean isConnect;
	@SerializedName("registedDate")
	@Expose
	public String registedDate;


	public boolean isChecked;


	@Setter
	@Getter
	@ToString
	@Parcel(value = Parcel.Serialization.BEAN, analyze = CanvasSetting.class)
	public static class CanvasSetting {
		@SerializedName("fit")
		@Expose
		public Fit fit;
		@SerializedName("data")
		@Expose
		public Data data;
		@SerializedName("style")
		@Expose
		public Style style;
		@SerializedName("caption")
		@Expose
		public Caption caption;
		@SerializedName("interval")
		@Expose
		public Interval interval;
		@SerializedName("transition")
		@Expose
		public Transition transition;

		@Setter
		@Getter
		@ToString
		@Parcel(value = Parcel.Serialization.BEAN, analyze = Caption.class)
		public static class Caption {
			@SerializedName("use")
			@Expose
			public Boolean use;
			@SerializedName("fontSize")
			@Expose
			public String fontSize;
			@SerializedName("fontColor")
			@Expose
			public String fontColor;
		}

		@Setter
		@Getter
		@ToString
		@Parcel(value = Parcel.Serialization.BEAN, analyze = Data.class)
		public static class Data {
			@SerializedName("type")
			@Expose
			public String type; // curation, pick

			@SerializedName("curationId")
			@Expose
			public Integer curationId;

			@SerializedName("pickId")
			@Expose
			public Integer pickId;

			@SerializedName("title")
			@Expose
			public String title;
		}

		@Setter
		@Getter
		@ToString
		@Parcel(value = Parcel.Serialization.BEAN, analyze = Fit.class)
		public static class Fit {
			@SerializedName("type")
			@Expose
			public String type;
		}

		@Setter
		@Getter
		@ToString
		@Parcel(value = Parcel.Serialization.BEAN, analyze = Interval.class)
		public static class Interval {
			@SerializedName("hour")
			@Expose
			public Integer hour;
			@SerializedName("minute")
			@Expose
			public Integer minute;
		}

		@Setter
		@Getter
		@ToString
		@Parcel(value = Parcel.Serialization.BEAN, analyze = Style.class)
		public static class Style {
			@SerializedName("backgroundColor")
			@Expose
			public String backgroundColor;
		}

		@Setter
		@Getter
		@ToString
		@Parcel(value = Parcel.Serialization.BEAN, analyze = Transition.class)
		public static class Transition {
			@SerializedName("type")
			@Expose
			public String type;
			@SerializedName("delay")
			@Expose
			public Integer delay;
			@SerializedName("duration")
			@Expose
			public Integer duration;
		}
	}

}

