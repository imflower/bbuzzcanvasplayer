package com.bbuzzart.canvas.network.model;

import com.bbuzzart.canvas.network.base.ServerResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Parcel(value = Parcel.Serialization.BEAN, analyze = Initialize.class)
@Getter
@Setter
@ToString
public class Initialize extends ServerResponse {

	@SerializedName("data")
	@Expose
	public Data data = new Data();

	@Getter
	@Setter
	@ToString
	@Parcel(value = Parcel.Serialization.BEAN, analyze = Data.class)
	public static class Data {

		@SerializedName("app")
		@Expose
		public App app;
		@SerializedName("cdn")
		@Expose
		public Cdn cdn;

		@Getter
		@Setter
		@ToString
		@Parcel(value = Parcel.Serialization.BEAN, analyze = App.class)
		public static class App {
			@SerializedName("build")
			@Expose
			public String build;
			@SerializedName("androidUrl")
			@Expose
			public String androidUrl;
			@SerializedName("isRequire")
			@Expose
			public Boolean isRequire;
			@SerializedName("message")
			@Expose
			public String message;
			@SerializedName("iosUrl")
			@Expose
			public String iosUrl;
			@SerializedName("version")
			@Expose
			public String version;
			@SerializedName("target")
			@Expose
			public Boolean target;
		}

		@Getter
		@Setter
		@ToString
		@Parcel(value = Parcel.Serialization.BEAN, analyze = Cdn.class)
		public static class Cdn {
			@SerializedName("domain")
			@Expose
			public String domain;
		}
	}

}
