package com.bbuzzart.canvas.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@Parcel(Parcel.Serialization.BEAN)
public class Pagination {

	@SerializedName("page")
	@Expose
	public Integer page;
	@SerializedName("size")
	@Expose
	public Integer size;
	@SerializedName("count")
	@Expose
	public Integer count;
	@SerializedName("totalPage")
	@Expose
	public Integer totalPage;

}
