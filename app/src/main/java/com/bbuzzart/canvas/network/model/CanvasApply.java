package com.bbuzzart.canvas.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@Parcel(Parcel.Serialization.BEAN)
@NoArgsConstructor
public class CanvasApply {

	@SerializedName("canvasIds")
	@Expose
	public int canvasIds[];

	@SerializedName("data")
	@Expose
	public Data data;


	@Setter
	@Getter
	@ToString
	@NoArgsConstructor
	@Parcel(value = Parcel.Serialization.BEAN, analyze = Data.class)
	public static class Data {
		@SerializedName("type")
		@Expose
		public String type;

		@SerializedName("curationId")
		@Expose
		public Integer curationId;

		@SerializedName("pickId")
		@Expose
		public Integer pickId;


		public Data(String type, int curationId, int pickId) {
			this.type = type;
			this.curationId = curationId;
			this.pickId = pickId;
		}
	}

	public CanvasApply(int canvasIds[], String type, int curationId, int pickId) {
		this.canvasIds = canvasIds;
		this.data = new Data(type, curationId, pickId);
	}

}
